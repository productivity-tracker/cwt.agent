﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using NuGet;
using Squirrel;
using Topshelf.Logging;
using Topshelf.Squirrel.Windows.Interfaces;

namespace Topshelf.Squirrel.Windows
{
	public class RepeatedTimeUpdater : IUpdater
	{
		private readonly LogWriter _log = HostLogger.Get<RepeatedTimeUpdater>();
		private TimeSpan checkUpdatePeriod = TimeSpan.FromSeconds(30);
		private readonly IUpdateManager updateManager;
		private string curversion;

		/// <summary>
		/// Задать время между проверками доступности обновлений. По умолчанию 30 секунд.
		/// </summary>
		/// <param name="checkSpan"></param>
		/// <returns></returns>
		public RepeatedTimeUpdater SetCheckUpdatePeriod(TimeSpan checkSpan)
		{
			checkUpdatePeriod = checkSpan;
			return this;
		}

		public RepeatedTimeUpdater(IUpdateManager updateManager)
		{
			if (!Environment.UserInteractive)
			{
				if (updateManager == null)
					throw new Exception("Update manager can not be null");
			}
			curversion = Assembly.GetEntryAssembly().GetName().Version.ToString();
			this.updateManager = updateManager;
		}
		
		/// <summary>
		/// Метод который проверяет обновления
		/// </summary>
		public void Start()
		{
			if (!Environment.UserInteractive)
			{
				Task.Run(Update).ConfigureAwait(false);
			}
		}

		private async Task Update()
		{
			if (updateManager == null)
				throw new Exception("Update manager can not be null");
            _log.InfoFormat($"Automatic-renewal was launched ({curversion})");

			{
				while (true)
				{
					await Task.Delay(checkUpdatePeriod);
					try
					{
						//Проверяем наличие новой версии
						var update = await updateManager.CheckForUpdate();
						try
						{
							var oldVersion = update.CurrentlyInstalledVersion?.Version ?? new SemanticVersion(0, 0, 0, 0);
							var newVersion = update.FutureReleaseEntry.Version;
							if (oldVersion < newVersion)
							{
                                _log.Info($"Found a new version: {newVersion}");

								//Скачиваем новую версию
								await updateManager.DownloadReleases(update.ReleasesToApply);

								//Распаковываем новую версию
								await updateManager.ApplyReleases(update);
							}
						}
						catch (Exception ex)
						{
                            _log.Error($"Error on update ({curversion})", ex);
						}
					}
					catch (Exception ex)
					{
                        _log.Error($"Error on check for update ({curversion})", ex);
					}
				}
			}
		}
	}
}