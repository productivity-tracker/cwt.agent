﻿namespace Topshelf.Squirrel.Windows.Interfaces
{
	public interface ISelfUpdatableService
	{
		void Start();
		void Stop();
        void OnSessionChange(SessionChangedArguments args);
    }
}