﻿using CWT.Agent.PlatformInfo.Domain.System;

namespace CWT.Agent.PlatformInfo.Interfaces.System
{
    public interface ISystemInfoService
    {
        DeviceInfo GetDeviceInfo();

        UserInfo GetUserInfo();

        string GetDeviceGuid();

        string GetUsername(int sessionId, bool prependDomain = true);
    }
}