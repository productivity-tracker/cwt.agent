﻿using System.Drawing;

namespace CWT.Agent.PlatformInfo.Interfaces
{
    public interface IPlatformService
    {
        Rectangle DesktopRectangle { get; }

        Point CursorPosition { get; }
    }
}