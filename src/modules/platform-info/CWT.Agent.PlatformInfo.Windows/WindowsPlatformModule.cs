﻿using Autofac;
using CWT.Agent.PlatformInfo.Windows.System;

namespace CWT.Agent.PlatformInfo.Windows
{
    public class WindowsPlatformModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WindowsPlatformService>().As<IPlatformService>();
            builder.RegisterType<WindowsSystemInfoService>().As<ISystemInfoService>();
        }
    }
}