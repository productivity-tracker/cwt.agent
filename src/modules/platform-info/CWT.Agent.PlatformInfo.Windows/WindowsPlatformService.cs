﻿using System.Drawing;
using System.Windows.Forms;
using CWT.Agent.Windows.Shared.Native;

namespace CWT.Agent.PlatformInfo.Windows
{
    public class WindowsPlatformService : IPlatformService
    {
        public Rectangle DesktopRectangle => SystemInformation.VirtualScreen;

        public Point CursorPosition
        {
            get
            {
                var p = new Point();
                User32.GetCursorPos(ref p);
                return p;
            }
        }
    }
}