﻿using System.Drawing;

namespace CWT.Agent.PlatformInfo.Windows
{
    public interface IPlatformService
    {
        Rectangle DesktopRectangle { get; }

        Point CursorPosition { get; }
    }
}