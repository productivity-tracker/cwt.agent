﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;

namespace CWT.Agent.PlatformInfo.Windows.System
{
    public class WindowsSystemInfoService : ISystemInfoService
    {
        [DllImport("Wtsapi32.dll")]
        private static extern bool WTSQuerySessionInformation(IntPtr hServer, int sessionId, WtsInfoClass wtsInfoClass, out IntPtr ppBuffer, out int pBytesReturned);
        [DllImport("Wtsapi32.dll")]
        private static extern void WTSFreeMemory(IntPtr pointer);

        private enum WtsInfoClass
        {
            WTSUserName = 5,
            WTSDomainName = 7,
        }

        private readonly ILogger _logger;

        public WindowsSystemInfoService(ILogger<WindowsSystemInfoService> logger)
        {
            _logger = logger;
        }

        public DeviceInfo GetDeviceInfo()
        {
            var uuid = GetDeviceGuid();
            var domain = IPGlobalProperties.GetIPGlobalProperties().DomainName;
            var deviceName = Environment.MachineName;
            var os = GetOsInfo();
            return new DeviceInfo(uuid, deviceName, domain, os);
        }

        public UserInfo GetUserInfo()
        {
            var (domain, username) = GetCurrentUser();
            var displayName = GetDisplayName(username);
            return new UserInfo(username, domain, displayName);
        }

        public string GetDeviceGuid()
        {
            const string location = @"SOFTWARE\Microsoft\Cryptography";
            const string name = "MachineGuid";

            using (var rk = GetRegistryKey(location))
            {
                if (rk == null)
                {
                    throw new KeyNotFoundException($"Key Not Found: {location}");
                }

                var machineGuid = rk.GetValue(name);
                if (machineGuid == null)
                {
                    throw new IndexOutOfRangeException($"Index Not Found: {name}");
                }

                return machineGuid.ToString();
            }
        }

        public string GetUsername(int sessionId, bool prependDomain = true)
        {
            var username = "SYSTEM";
            if (WTSQuerySessionInformation(IntPtr.Zero, sessionId, WtsInfoClass.WTSUserName, out var buffer, out var strLen) && strLen > 1)
            {
                username = Marshal.PtrToStringAnsi(buffer);
                WTSFreeMemory(buffer);
                if (prependDomain)
                {
                    if (WTSQuerySessionInformation(IntPtr.Zero, sessionId, WtsInfoClass.WTSDomainName, out buffer, out strLen) && strLen > 1)
                    {
                        username = Marshal.PtrToStringAnsi(buffer) + "\\" + username;
                        WTSFreeMemory(buffer);
                    }
                }
            }

            return username;
        }

        private (string domain, string username) GetCurrentUser()
        {
            var result = (domain: string.Empty, username: string.Empty);
            try
            {
                var ms = new ManagementScope("\\\\.\\root\\cimv2");
                ms.Connect();

                var query = new ObjectQuery("SELECT * FROM Win32_ComputerSystem");
                var searcher = new ManagementObjectSearcher(ms, query);
                var username = string.Empty;

                foreach (var mo in searcher.Get())
                {
                    username = mo["UserName"].ToString();
                }

                var usernameParts = username.Split('\\');
                if (usernameParts.Length >= 2)
                {
                    result.domain = usernameParts[0];
                    result.username = usernameParts[1];
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occurred while get current user via WMI");
            }

            return result;
        }

        private string GetDisplayName(string username)
        {
            const string location = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI";
            const string displayNameKey = "LastLoggedOnDisplayName";
            const string samUserKey = "LastLoggedOnSAMUser";

            using (var authInfo = GetRegistryKey(location))
            {
                var lastLoggedOnSamUser = authInfo?.GetValue(samUserKey).ToString();
                if (lastLoggedOnSamUser?.Contains(username) != true)
                {
                    return null;
                }

                var displayName = authInfo.GetValue(displayNameKey);
                return displayName.ToString();
            }
        }

        private RegistryKey GetRegistryKey(string name)
        {
            var localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,
                                                   Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32);
            return localKey.OpenSubKey(name);
        }

        private string GetOsInfo()
        {
            var result = string.Empty;
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem"))
            {
                var information = searcher.Get();
                foreach (var obj in information)
                {
                    result = $"{obj["Caption"]} ({obj["OSArchitecture"]})";
                }
            }

            result = result.Replace("NT 5.1.2600", "XP");
            result = result.Replace("NT 5.2.3790", "Server 2003");
            return result;
        }
    }
}
