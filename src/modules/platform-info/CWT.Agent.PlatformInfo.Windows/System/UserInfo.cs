﻿using CWT.Agent.Base.Extensions;

namespace CWT.Agent.PlatformInfo.Windows.System
{
    public class UserInfo
    {
        public bool IsLocalUser => string.IsNullOrWhiteSpace(Domain) || Domain.Equals(".");
        public string UserName { get; set; }
        public string Domain { get; set; }
        public string DisplayName { get; set; }

        public UserInfo(string userName, string domain, string displayName)
        {
            UserName = userName.ToLatin();
            Domain = domain.ToLatin();
            DisplayName = displayName.ToLatin();
        }
    }
}