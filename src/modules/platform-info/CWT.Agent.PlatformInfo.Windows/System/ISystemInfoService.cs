﻿namespace CWT.Agent.PlatformInfo.Windows.System
{
    public interface ISystemInfoService
    {
        DeviceInfo GetDeviceInfo();

        UserInfo GetUserInfo();

        string GetDeviceGuid();

        string GetUsername(int sessionId, bool prependDomain = true);
    }
}