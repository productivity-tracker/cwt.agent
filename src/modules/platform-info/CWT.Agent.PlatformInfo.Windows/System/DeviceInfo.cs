﻿using CWT.Agent.Base.Extensions;

namespace CWT.Agent.PlatformInfo.Windows.System
{
    public class DeviceInfo
    {
        public string UniversallyUniqueId { get; set; }
        public string DeviceName { get; set; }
        public string Domain { get; set; }
        public string OperatingSystem { get; set; }

        public DeviceInfo(string universallyUniqueId, string deviceName, string domain, string operatingSystem)
        {
            UniversallyUniqueId = universallyUniqueId;
            DeviceName = deviceName.ToLatin();
            Domain = domain.ToLatin();
            OperatingSystem = operatingSystem;
        }
    }
}
