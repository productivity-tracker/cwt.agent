﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CWT.Agent.ProcessMonitoring
{
    public interface IProcessMonitor : IDisposable
    {
        IObservable<IList<ProcessInfo>> ProcessListChanged { get; }
        Task Run(IEnumerable<string> processFilters);
    }
}