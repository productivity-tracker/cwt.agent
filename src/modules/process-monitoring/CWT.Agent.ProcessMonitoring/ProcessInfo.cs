﻿using System;

namespace CWT.Agent.ProcessMonitoring
{
    public class ProcessInfo : IEquatable<ProcessInfo>
    {
        public int Id { get; }
        public string Name { get; }
        public int SessionId { get; }

        public ProcessInfo(int id, string name, int sessionId)
        {
            Id = id;
            Name = name;
            SessionId = sessionId;
        }

        #region IEquatable Support

        public bool Equals(ProcessInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && Name == other.Name && SessionId == other.SessionId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ProcessInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ SessionId;
                return hashCode;
            }
        }

        public static bool operator ==(ProcessInfo left, ProcessInfo right) => Equals(left, right);

        public static bool operator !=(ProcessInfo left, ProcessInfo right) => !Equals(left, right);

        #endregion
    }
}