﻿namespace CWT.Agent.ProcessMonitoring
{
    public enum ProcessStateChangedReason
    {
        Started = 1,
        Terminated
    }
}