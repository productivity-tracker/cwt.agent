﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace CWT.Agent.ProcessMonitoring
{
    public class MultiProcessMonitor : IDisposable
    {
        private readonly ILogger _logger;

        private readonly ISet<string> _observableProcesses;
        private readonly ISubject<ProcessStateChange> _internalSubjects;
        public IObservable<ProcessStateChange> StateChanges => _internalSubjects;

        private readonly ManagementEventWatcher _startWatcher;
        private readonly ManagementEventWatcher _stopWatcher;
        private readonly object _syncStream = new object();

        public MultiProcessMonitor(ILoggerFactory loggerFactory, ISet<string> observableProcesses)
        {
            _logger = loggerFactory?.CreateLogger<MultiProcessMonitor>() ?? NullLogger<MultiProcessMonitor>.Instance;
            _observableProcesses = observableProcesses;
            _internalSubjects = new Subject<ProcessStateChange>();

            _startWatcher = new ManagementEventWatcher("Select * From Win32_ProcessStartTrace");
            _stopWatcher = new ManagementEventWatcher("Select * From Win32_ProcessStopTrace");
        }

        public void Start()
        {
            lock (_syncStream)
            {
                GetRunningProcessesBySession(Process.GetCurrentProcess().SessionId)
                    .Where(p => _observableProcesses.Any(pattern => Regex.IsMatch(p.ProcessName, pattern)))
                    .ToObservable()
                    .Subscribe(process =>
                                   _internalSubjects.OnNext(new ProcessStateChange(
                                                                process.ProcessName,
                                                                process.Id,
                                                                ProcessStateChangedReason.Started)));

                _startWatcher.EventArrived += OnProcessStarted;
                _stopWatcher.EventArrived += OnProcessTerminated;
            }
        }

        public void Dispose()
        {
            _internalSubjects.OnCompleted();
            _startWatcher.EventArrived -= OnProcessStarted;
            _stopWatcher.EventArrived -= OnProcessTerminated;
            _startWatcher?.Dispose();
            _stopWatcher?.Dispose();
        }

        private IEnumerable<Process> GetRunningProcessesBySession(int sessionId)
        {
            try
            {
                return Process.GetProcesses().Where(p => p.SessionId == sessionId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return new List<Process>();
            }
        }

        private void OnProcessStarted(object sender, EventArrivedEventArgs e)
            => OnProcessChanged(e, ProcessStateChangedReason.Started);

        private void OnProcessTerminated(object s, EventArrivedEventArgs e)
            => OnProcessChanged(e, ProcessStateChangedReason.Terminated);

        private void OnProcessChanged(EventArrivedEventArgs e, ProcessStateChangedReason reason)
        {
            lock (_syncStream)
            {
                var processName = (string)e.NewEvent["ProcessName"];
                if (!_observableProcesses.Any(pattern => Regex.IsMatch(processName, pattern))) return;

                _internalSubjects.OnNext(new ProcessStateChange(processName, (int)e.NewEvent["ProcessID"], reason));
                _logger.LogDebug($"The process '{processName}' was started.");
            }
        }
    }
}