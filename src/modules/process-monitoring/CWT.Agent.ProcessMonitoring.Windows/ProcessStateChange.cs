﻿namespace CWT.Agent.ProcessMonitoring
{
    public class ProcessStateChange
    {
        public int ProcessId { get; }
        public string Name { get; }
        public ProcessStateChangedReason Reason { get; }

        public ProcessStateChange(string name, int processId, ProcessStateChangedReason reason)
        {
            Name = name;
            ProcessId = processId;
            Reason = reason;
        }
    }
}