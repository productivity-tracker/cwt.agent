﻿using Autofac;
using CWT.Agent.ScreenCapture.Capture;
using CWT.Agent.ScreenCapture.Capture.GDI;

namespace CWT.Agent.ScreenCapture
{
    public class ScreenCaptureWindowsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GdiCaptureService>().As<IScreenCaptureService>();
        }
    }
}