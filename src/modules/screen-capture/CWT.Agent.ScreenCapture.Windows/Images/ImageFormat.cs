﻿namespace CWT.Agent.ScreenCapture.Images
{
    public enum ImageFormat
    {
        Jpg,
        Png,
        Gif,
        Bmp
    }
}