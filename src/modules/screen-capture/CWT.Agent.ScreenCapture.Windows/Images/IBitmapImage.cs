﻿using System;
using System.IO;

namespace CWT.Agent.ScreenCapture.Images
{
    public interface IBitmapImage : IDisposable
    {
        int Width { get; }

        int Height { get; }

        DateTime CreatedAtUtc { get; }

        void Save(string fileName, ScreenCapture.Images.ImageFormat format);

        void Save(Stream stream, ScreenCapture.Images.ImageFormat format, long quality = 100L);

        IBitmapImage Copy();
    }
}