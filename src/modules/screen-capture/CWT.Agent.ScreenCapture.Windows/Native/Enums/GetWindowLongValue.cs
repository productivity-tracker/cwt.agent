﻿namespace CWT.Agent.ScreenCapture.Windows.Native.Enums
{
    internal enum GetWindowLongValue
    {
        Style = -16,
        ExStyle = -20
    }
}