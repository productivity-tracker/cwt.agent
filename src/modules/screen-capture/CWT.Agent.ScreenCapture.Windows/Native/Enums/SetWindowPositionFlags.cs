﻿namespace CWT.Agent.ScreenCapture.Windows.Native.Enums
{
    internal enum SetWindowPositionFlags
    {
        ShowWindow = 0x400,
        NoActivate = 0x0010
    }
}