﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using CWT.Agent.ScreenCapture.Capture.GDI;
using CWT.Agent.Windows.Shared.Native;
using CWT.Agent.Windows.Shared.Native.Enums;
using CWT.Agent.Windows.Shared.Native.Structs;

namespace CWT.Agent.ScreenCapture.Capture
{
/// <summary>
    ///     Draws the MouseCursor on an Image
    /// </summary>
    internal static class MouseCursor
    {
        private const int CURSOR_SHOWING = 1;

        /// <summary>
        ///     Draws this overlay.
        /// </summary>
        /// <param name="g">A <see cref="Graphics" /> object to draw upon.</param>
        /// <param name="transform">Point Transform Function.</param>
        public static void Draw(Graphics g, Func<Point, Point> transform = null)
        {
            var hIcon = GetIcon(transform, out var location);

            if (hIcon == IntPtr.Zero)
                return;

            var bmp = Icon.FromHandle(hIcon).ToBitmap();
            User32.DestroyIcon(hIcon);

            try
            {
                using (bmp)
                {
                    g.DrawImage(bmp, new Rectangle(location, bmp.Size));
                }
            }
            catch (ArgumentException)
            {
            }
        }

        public static void Draw(IntPtr deviceContext, Func<Point, Point> transform = null)
        {
            var hIcon = GetIcon(transform, out var location);

            if (hIcon == IntPtr.Zero)
                return;

            try
            {
                User32.DrawIconEx(deviceContext, location.X, location.Y, hIcon, 0, 0, 0, IntPtr.Zero, DrawIconExFlags.Normal);
            }
            finally
            {
                User32.DestroyIcon(hIcon);
            }
        }

        private static IntPtr GetIcon(Func<Point, Point> transform, out Point location)
        {
            location = Point.Empty;

            var cursorInfo = new CursorInfo { cbSize = Marshal.SizeOf<CursorInfo>() };

            if (!User32.GetCursorInfo(ref cursorInfo))
                return IntPtr.Zero;

            if (cursorInfo.flags != CURSOR_SHOWING)
                return IntPtr.Zero;

            var hIcon = User32.CopyIcon(cursorInfo.hCursor);

            if (hIcon == IntPtr.Zero)
                return IntPtr.Zero;

            if (!User32.GetIconInfo(hIcon, out var icInfo))
                return IntPtr.Zero;

            var hotspot = new Point(icInfo.xHotspot, icInfo.yHotspot);

            location = new Point(cursorInfo.ptScreenPos.X - hotspot.X, cursorInfo.ptScreenPos.Y - hotspot.Y);

            if (transform != null)
                location = transform(location);

            Gdi32.DeleteObject(icInfo.hbmColor);
            Gdi32.DeleteObject(icInfo.hbmMask);

            return hIcon;
        }
    }
}