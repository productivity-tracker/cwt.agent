﻿using System.Drawing;
using IBitmapImage = CWT.Agent.ScreenCapture.Images.IBitmapImage;

namespace CWT.Agent.ScreenCapture.Capture
{
    public interface IScreenCaptureService
    {
        bool IsPlatformSupported { get; }
        IBitmapImage Capture(Rectangle region, bool includeCursor = false);
    }
}