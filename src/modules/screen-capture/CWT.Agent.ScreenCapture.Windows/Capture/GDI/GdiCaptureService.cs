﻿using System.Drawing;
using CWT.Agent.ScreenCapture.Images;

namespace CWT.Agent.ScreenCapture.Capture.GDI
{
    public class GdiCaptureService : IScreenCaptureService
    {
        public bool IsPlatformSupported { get; } = true; //GDI is supported everywhere

        public IBitmapImage Capture(Rectangle region, bool includeCursor = false) 
            => ScreenShot.Capture(region, includeCursor);
    }
}