﻿using System;
using System.Runtime.InteropServices;

namespace CWT.Agent.ScreenCapture.Capture.GDI
{
    internal static class Gdi32
    {
        private const string DLL_NAME = "gdi32.dll";

        [DllImport(DLL_NAME)]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport(DLL_NAME)]
        public static extern bool BitBlt(
            IntPtr hObject,
            int xDest,
            int yDest,
            int width,
            int height,
            IntPtr objectSource,
            int xSrc,
            int ySrc,
            int op);

        [DllImport(DLL_NAME)]
        public static extern bool StretchBlt(
            IntPtr hObject,
            int xDest,
            int yDest,
            int wDest,
            int hDest,
            IntPtr objectSource,
            int xSrc,
            int ySrc,
            int wSrc,
            int hSrc,
            int op);

        [DllImport(DLL_NAME)]
        public static extern IntPtr CreateCompatibleBitmap(IntPtr hDc, int width, int height);

        [DllImport(DLL_NAME)]
        public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

        [DllImport(DLL_NAME)]
        public static extern bool DeleteDC(IntPtr hDC);

        [DllImport(DLL_NAME)]
        public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
    }
}