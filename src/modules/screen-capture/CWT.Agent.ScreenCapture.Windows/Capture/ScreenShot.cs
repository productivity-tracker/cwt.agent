﻿using System.Drawing;
using CWT.Agent.ScreenCapture.Drawing;
using IBitmapImage = CWT.Agent.ScreenCapture.Images.IBitmapImage;

namespace CWT.Agent.ScreenCapture.Capture
{
    /// <summary>
    ///     Contains methods for taking ScreenShots
    /// </summary>
    public static class ScreenShot
    {
        /// <summary>
        ///     Captures a Specific Region.
        /// </summary>
        /// <param name="region">A <see cref="Rectangle" /> specifying the Region to Capture.</param>
        /// <param name="includeCursor">Whether to include the Mouse Cursor.</param>
        /// <returns>The Captured Image.</returns>
        public static IBitmapImage Capture(Rectangle region, bool includeCursor = false)
        {
            return new DrawingImage(CaptureInternal(region, includeCursor));
        }

        private static Bitmap CaptureInternal(Rectangle region, bool includeCursor = false)
        {
            var bmp = new Bitmap(region.Width, region.Height);

            using (var g = Graphics.FromImage(bmp))
            {
                g.CopyFromScreen(region.Location, Point.Empty, region.Size, CopyPixelOperation.SourceCopy);

                if (includeCursor)
                    MouseCursor.Draw(g, p => new Point(p.X - region.X, p.Y - region.Y));

                g.Flush();
            }

            return bmp;
        }
    }
}