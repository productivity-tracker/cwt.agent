﻿using System.Drawing;
using ImageFormat = CWT.Agent.ScreenCapture.Images.ImageFormat;

namespace CWT.Agent.ScreenCapture.Drawing
{
    public static class GraphicsExtensions
    {
        public static Rectangle Even(this Rectangle rect)
        {
            if (rect.Width % 2 == 1)
                --rect.Width;

            if (rect.Height % 2 == 1)
                --rect.Height;

            return rect;
        }

        public static System.Drawing.Imaging.ImageFormat ToDrawingImageFormat(this ImageFormat format)
        {
            switch (format)
            {
                case ImageFormat.Jpg: return System.Drawing.Imaging.ImageFormat.Jpeg;

                case ImageFormat.Png: return System.Drawing.Imaging.ImageFormat.Png;

                case ImageFormat.Gif: return System.Drawing.Imaging.ImageFormat.Gif;

                case ImageFormat.Bmp: return System.Drawing.Imaging.ImageFormat.Bmp;

                default: return System.Drawing.Imaging.ImageFormat.Png;
            }
        }
    }
}