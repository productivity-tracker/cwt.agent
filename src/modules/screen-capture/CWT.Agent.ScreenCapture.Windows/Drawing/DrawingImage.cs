﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using IBitmapImage = CWT.Agent.ScreenCapture.Images.IBitmapImage;
using ImageFormat = CWT.Agent.ScreenCapture.Images.ImageFormat;

namespace CWT.Agent.ScreenCapture.Drawing
{
    public class DrawingImage : IBitmapImage
    {
        public int Width => Image.Width;
        public int Height => Image.Height;
        public Image Image { get; }
        public DateTime CreatedAtUtc { get; }

        public DrawingImage(Image image) : this(image, DateTime.UtcNow)
        { }

        private DrawingImage(Image image, DateTime dateTime)
        {
            CreatedAtUtc = dateTime;
            Image = image;
        }

        public void Dispose()
        {
            Image.Dispose();
        }

        public void Save(string fileName, ImageFormat format)
        {
            Image.Save(fileName, format.ToDrawingImageFormat());
        }

        public void Save(Stream stream, ImageFormat format, long quality = 100L)
        {
            // ReSharper disable once UseObjectOrCollectionInitializer
            var encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
            Image.Save(stream, GetEncoder(format.ToDrawingImageFormat()), encoderParameters);
        }

        public IBitmapImage Copy() => new DrawingImage((Image)Image.Clone(), CreatedAtUtc);

        private ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
        {
            var codecs = ImageCodecInfo.GetImageDecoders();
            return codecs.FirstOrDefault(codec => codec.FormatID == format.Guid);
        }
    }
}