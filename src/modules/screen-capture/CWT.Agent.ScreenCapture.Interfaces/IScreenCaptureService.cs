﻿using System.Drawing;
using CWT.Agent.ScreenCapture.Interfaces.Images;

namespace CWT.Agent.ScreenCapture.Interfaces
{
    public interface IScreenCaptureService
    {
        bool IsPlatformSupported { get; }
        IBitmapImage Capture(Rectangle region, bool includeCursor = false);
    }
}