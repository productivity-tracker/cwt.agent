﻿using System;
using System.IO;

namespace CWT.Agent.ScreenCapture.Interfaces.Images
{
    public interface IBitmapImage : IDisposable
    {
        int Width { get; }

        int Height { get; }

        DateTime CreatedAtUtc { get; }

        void Save(string fileName, ImageFormat format);

        void Save(Stream stream, ImageFormat format, long quality = 100L);

        IBitmapImage Copy();
    }
}