﻿namespace CWT.Agent.ScreenCapture.Interfaces.Images
{
    public enum ImageFormat
    {
        Jpg,
        Png,
        Gif,
        Bmp
    }
}