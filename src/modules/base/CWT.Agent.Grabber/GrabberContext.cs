﻿namespace CWT.Agent.Grabber
{
    public class GrabberContext
    {
        public string UserAgentId { get; private set; }
        public uint SessionId { get; private set; }

        public GrabberContext(uint sessionId, string userAgentId)
        {
            SessionId = sessionId;
            UserAgentId = userAgentId;
        }
    }
}