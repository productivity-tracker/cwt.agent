﻿using System;
using System.Reactive.Linq;

namespace CWT.Agent.Base.Extensions
{
    public static class RX
    {
        public static IObservable<T> RepeatLastValueDuringSilence<T>(this IObservable<T> inner, TimeSpan maxQuietPeriod) 
            => inner.Select(
                        x =>
                            Observable.Interval(maxQuietPeriod)
                                      .Select(_ => x)
                                      .StartWith(x))
                    .Switch();

        public static IObservable<T> RepeatLastValueUntil<T>(
            this IObservable<T> inner,
            TimeSpan interval,
            Predicate<T> stopPredicate)
            => inner.Select(
                        x => Observable.Interval(interval)
                                       .TakeUntil(inner)
                                       .TakeUntil(_ => stopPredicate(x))
                                       .Select(_ => x))
                    .Switch();
    }
}