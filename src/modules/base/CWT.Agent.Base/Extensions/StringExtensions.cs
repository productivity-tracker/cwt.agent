﻿namespace CWT.Agent.Base.Extensions
{
    public static class StringExtensions
    {
        public static string ToLatin(this string text)
        {
            return Transliteration.CheckTextInCyrillic(text) ? Transliteration.Front(text, TransliterationType.ISO) : text;
        }
    }
}