﻿namespace CWT.Agent.Base
{
    public class SettingsHolder<T>
    {
        public string UserAgentId { get; private set; }
        public T Value { get; private set; }

        public SettingsHolder(string userAgentId, T value)
        {
            UserAgentId = userAgentId;
            Value = value;
        }
    }
}