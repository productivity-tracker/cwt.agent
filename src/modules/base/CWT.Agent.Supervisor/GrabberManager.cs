﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using CWT.Agent.ProcessManagement.Windows;
using CWT.Agent.Supervisor.Domain;
using CWT.Agent.Supervisor.Domain.Sessions;
using CWT.Agent.Supervisor.Domain.UserSessionRegistration;
using LiteDB;
using Microsoft.Extensions.Hosting;
using NLog;

namespace CWT.Agent.Supervisor.Application
{
    public class GrabberManager : IHostedService
    {
        private enum ResultCode
        {
            None = 0,
            RunSuccess = 1,
            StopSuccess = 2,
            RunFail = 4,
            StopFail = 8,
            Success = RunSuccess | StopSuccess,
            Fail = RunFail | StopFail
        }

        private readonly string _applicationName;
        private readonly string _applicationFullPath;

        private readonly ILiteRepository _repository;
        private readonly ISupervisorContext _context;

        private readonly ActionBlock<SessionArg> _actionBlock;

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private IDisposable _sessionSubscription;

        private volatile int _lastResult;

        private ResultCode LastResultCode
        {
            get => (ResultCode)Interlocked.CompareExchange(ref _lastResult, 0, 0);
            set => Interlocked.Exchange(ref _lastResult, (int)value);
        }

        public GrabberManager(
            ISupervisorContext context, 
            string applicationPath,
            ILiteRepository repository, 
            CancellationToken cancellationToken)
        {
            _context = context;
            _repository = repository;
            _applicationFullPath = applicationPath;
            _applicationName = Path.GetFileName(applicationPath);

            _actionBlock = new ActionBlock<SessionArg>(
                Handler,
                new ExecutionDataflowBlockOptions
                {
                    SingleProducerConstrained = true,
                    CancellationToken = cancellationToken
                });
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _sessionSubscription = _context.SessionChanges
                                           .FirstAsync() // for immediate handling first change
                                           .Merge(_context.SessionChanges
                                                          .Where(s => s.Reason != null)
                                                          .RepeatLastValueUntil(
                                                              TimeSpan.FromSeconds(20),
                                                              _ => ResultCode.Success.HasFlag(LastResultCode)))
                                           .Subscribe(_actionBlock.AsObserver());
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _sessionSubscription?.Dispose();
            _actionBlock.Complete();
            return Task.CompletedTask;
        }

        private void Handler(SessionArg sessionArg)
        {
            var resultCode = ResultCode.None;
            if (sessionArg.Reason is null)
            {
                resultCode = KillAllRunningGrabbers() ? ResultCode.StopSuccess : ResultCode.StopFail;
            }
            else
            {
                switch (sessionArg.Reason)
                {
                    case SessionChangeReason.SessionLogon:
                    case SessionChangeReason.SessionUnlock:
                        resultCode = RunGrabber(sessionArg.SessionId, sessionArg.User.UserName);
                        break;
                    case SessionChangeReason.SessionLogoff:
                    case SessionChangeReason.SessionLock:
                        resultCode = KillGrabberBySessionId(sessionArg.SessionId, sessionArg.User.UserName);
                        break;
                }
            }

            if (resultCode != ResultCode.None)
                LastResultCode = resultCode;
        }

        private ResultCode RunGrabber(uint sessionId, string userName, bool relaunch = false)
        {
            if (IsRunning(sessionId) && !relaunch) return ResultCode.None;
            if (relaunch)
            {
                KillGrabberBySessionId(sessionId, userName);
            }
            else
            {
                KillAllRunningGrabbers();
            }

            var userAgentId = _repository.FirstOrDefault<UserIdentifier>(
                x => x.UserName == userName.ToLower())?.RegisterId;
            if (userAgentId.IsNullOrWhiteSpace())
            {
                _logger.Error($"Error occurred while try to start {_applicationName}");
                return ResultCode.RunFail;
            }

            var commandLine = $"{_applicationFullPath} --u {userAgentId}";
            try
            {
                var result = ApplicationLauncher.StartProcessAndBypassUAC(commandLine, out _);
                return result ? ResultCode.RunSuccess : ResultCode.RunFail;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred while run Grabber");
                return ResultCode.RunFail;
            }
        }

        private bool KillAllRunningGrabbers()
        {
            try
            {
                var processes = Process.GetProcessesByName(_applicationName);
                foreach (var process in processes)
                {
                    process.Kill();
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        private ResultCode KillGrabberBySessionId(uint sessionId, string userName)
            => KillGrabberProcess(GetGrabberProcess(sessionId), userName) ? ResultCode.StopSuccess : ResultCode.StopFail;

        private bool KillGrabberProcess(Process process, string userName)
        {
            bool result;
            try
            {
                if (process == null) return false;

                process.Kill();
                _logger.Debug($"Grabber (processId: {process.Id}, username: {userName}) closed.");
                result = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred while close Grabber");
                result = false;
            }

            return result;
        }

        private Process GetGrabberProcess(uint sessionId)
            => Process.GetProcessesByName(_applicationName).FirstOrDefault(p => p.SessionId == sessionId);

        private bool IsRunning(uint sessionId)
            => GetGrabberProcess(sessionId) != null;
    }
}