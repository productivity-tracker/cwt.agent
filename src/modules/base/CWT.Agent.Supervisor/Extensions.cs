﻿using System;
using System.Reactive.Linq;

namespace CWT.Agent.Supervisor.Application
{
    public static class Extensions
    {
        public static IObservable<T> RepeatLastValueUntil<T>(
            this IObservable<T> inner,
            TimeSpan interval,
            Predicate<T> stopPredicate)
            => inner.Select(
                        x => Observable.Interval(interval)
                                       .TakeUntil(inner)
                                       .TakeUntil(_ => stopPredicate(x))
                                       .Select(_ => x))
                    .Switch();
    }
}