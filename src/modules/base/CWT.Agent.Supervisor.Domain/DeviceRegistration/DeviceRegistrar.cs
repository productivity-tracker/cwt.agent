using System;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using AgentIdentity.Application.Contracts.Devices.Identity;
using AgentIdentity.Application.Contracts.Devices.Identity.DTO;
using AgentIdentity.Domain.Shared.Device;
using CWT.Agent.Base;
using CWT.Agent.PlatformInfo.Windows.System;
using CWT.Agent.Supervisor.Domain.Sessions;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using Polly;
using ILogger = NLog.ILogger;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace CWT.Agent.Supervisor.Domain.DeviceRegistration
{
    public class DeviceRegistrar : IHostedService
    {
        private class ForeverReconnectPolicy : IRetryPolicy
        {
            public TimeSpan SleepDuration { get; private set; }

            public ForeverReconnectPolicy(TimeSpan sleepDuration)
            {
                SleepDuration = sleepDuration;
            }

            public TimeSpan? NextRetryDelay(RetryContext retryContext) => SleepDuration;
        }

        private readonly ISupervisorContext _context;
        private readonly IDeviceIdentityService _deviceIdentityService;
        private readonly ISystemInfoService _systemInfoService;

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private readonly HubConnection _hubConnection;

        private readonly ActionBlock<SessionArg> _actionBlock;

        private IDisposable _subscription;

        private string _deviceId;

        /// <summary>
        /// Current device id
        /// </summary>
        public string DeviceId
        {
            get => _deviceId;
            set => Interlocked.Exchange(ref _deviceId, value);
        }

        public DeviceRegistrar(
            ISupervisorContext context,
            IDeviceIdentityService deviceIdentityService,
            ISystemInfoService systemInfoService,
            string hubUrl,
            CancellationToken cancellationToken = default)
        {
            _systemInfoService = systemInfoService;
            _deviceIdentityService = deviceIdentityService;
            _context = context;

            _hubConnection = new HubConnectionBuilder()
                             .WithAutomaticReconnect(new ForeverReconnectPolicy(TimeSpan.FromSeconds(20)))
                             .ConfigureLogging(logBuilder => logBuilder.SetMinimumLevel(LogLevel.Trace))
                             .WithUrl(
                                 hubUrl,
                                 opt => opt.Headers.Add("GUID", _systemInfoService.GetDeviceGuid()))
                             .Build();

            _actionBlock = new ActionBlock<SessionArg>(
                TryRegisterState,
                new ExecutionDataflowBlockOptions
                {
                    SingleProducerConstrained = true,
                    CancellationToken = cancellationToken,
                    BoundedCapacity = 15
                });
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await Policy.Handle<Exception>()
                  .WaitAndRetryForeverAsync(_ => TimeSpan.FromSeconds(20),
                      (ex, _) => _logger.Debug(ex, "Error occurred while establishing connection. "))
                  .ExecuteAsync(ct => _hubConnection.StartAsync(ct), cancellationToken);

            DeviceId = await PersistentRegisterDevice(cancellationToken);

            _hubConnection.Reconnected += OnReconnected;

            _subscription = _context.SessionChanges
                                    .Where(IsChangeDeviceStatus)
                                    .Subscribe(_actionBlock.AsObserver());
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _hubConnection.Reconnected -= OnReconnected;

            _subscription?.Dispose();
            await _hubConnection.StopAsync(cancellationToken);
            _actionBlock.Complete();
        }
        private async Task OnReconnected(string arg)
        {
            DeviceId = await PersistentRegisterDevice();
        }

        private async Task TryRegisterState(SessionArg arg)
        {
            try
            {
                await RegisterState(arg.Reason);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Error occurred while {nameof(TryRegisterState)}");
            }
        }

        private Task<string> PersistentRegisterDevice(CancellationToken cancellation = default)
            => Policy.Handle<Exception>()
                     .OrResult<string>(x => x.IsNullOrWhiteSpace())
                           .WaitAndRetryForeverAsync(
                               retryAttempt => TimeSpan.FromSeconds(10),
                               (outcome, _) =>
                               {
                                   if(outcome.Exception != null)
                                       _logger.Debug(outcome.Exception, "Error occurrd while register device.");
                               })
                           .ExecuteAsync(
                               _ =>
                               {
                                   if (_hubConnection.State != HubConnectionState.Connected) return null;
                               
                                   var device = _systemInfoService.GetDeviceInfo();
                                   return _deviceIdentityService.RegisterAsync(
                                       new RegistrationRequest(_hubConnection.ConnectionId,
                                                               device.UniversallyUniqueId,
                                                               device.DeviceName,
                                                               device.Domain,
                                                               device.OperatingSystem,
                                                               AssemblyHelper.AssemblyVersion,
                                                               "", // TODO: input IP, or fill on server side
                                                               DeviceType.PC));
                               }, cancellation);

        private bool IsChangeDeviceStatus(SessionArg arg)
            => arg.Reason.IsIn(
                SessionChangeReason.SessionLock,
                SessionChangeReason.SessionUnlock);

        private Task RegisterState(SessionChangeReason? reason)
        {
            if(DeviceId.IsNullOrWhiteSpace()) return Task.CompletedTask;

            var request = new ChangeStateRequest
            {
                UniversallyUniqueDeviceId = DeviceId,
                NewState = Translate(reason)
            };
            return _deviceIdentityService.PutStateAsync(request);
        }

        private DeviceState Translate(SessionChangeReason? reason) => reason switch
        {
            SessionChangeReason.SessionUnlock => DeviceState.Unlocked,
            SessionChangeReason.SessionLock => DeviceState.Locked,
            _ => DeviceState.Unknown
        };
    }
}