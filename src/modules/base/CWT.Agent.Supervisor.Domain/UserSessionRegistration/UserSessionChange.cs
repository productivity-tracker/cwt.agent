﻿using System;
using CWT.Agent.Supervisor.Domain.Sessions;

namespace CWT.Agent.Supervisor.Domain.UserSessionRegistration
{
    public class UserSessionChange : IEquatable<UserSessionChange>
    {
        public string UserName { get; private set; }
        public string Domain { get; private set; }
        public bool IsLocalUser { get; private set; }
        public string DeviceUUID { get; private set; }
        public SessionChangeReason Reason { get; private set; }

        public UserSessionChange(
            string deviceUuid,
            string domain,
            bool isLocalUser,
            string userName,
            SessionChangeReason reason)
        {
            DeviceUUID = deviceUuid;
            Domain = domain;
            IsLocalUser = isLocalUser;
            UserName = userName;
            Reason = reason;
        }

        #region IEquatable Support

        public bool Equals(UserSessionChange other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return UserName == other.UserName
                && Domain == other.Domain
                && IsLocalUser == other.IsLocalUser
                && DeviceUUID == other.DeviceUUID
                && Reason == other.Reason;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((UserSessionChange)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = UserName != null ? UserName.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (Domain != null ? Domain.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsLocalUser.GetHashCode();
                hashCode = (hashCode * 397) ^ (DeviceUUID != null ? DeviceUUID.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)Reason;
                return hashCode;
            }
        }

        public static bool operator ==(UserSessionChange left, UserSessionChange right) => Equals(left, right);

        public static bool operator !=(UserSessionChange left, UserSessionChange right) => !Equals(left, right);

        #endregion
    }
}