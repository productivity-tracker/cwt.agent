﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using AgentIdentity.Application.Contracts.UserAgents.Auth;
using AgentIdentity.Application.Contracts.UserAgents.Auth.DTO;
using CWT.Agent.PlatformInfo.Windows.System;
using CWT.Agent.Supervisor.Domain.Sessions;
using LiteDB;
using Microsoft.Extensions.Hosting;
using Polly;

namespace CWT.Agent.Supervisor.Domain.UserSessionRegistration
{
    public class UserRegistrar : IHostedService
    {
        private readonly IUserAgentAuthService _userAgentAuthService;
        private readonly ISystemInfoService _systemInfoService;
        private readonly ILiteRepository _repository;
        private readonly ISupervisorContext _context;

        private readonly ActionBlock<SessionArg> _actionBlock;

        private IDisposable _subscription;

        public UserRegistrar(
            ILiteRepository repository,
            ISupervisorContext context,
            IUserAgentAuthService userAgentAuthService,
            ISystemInfoService systemInfoService,
            CancellationToken cancellationToken)
        {
            _repository = repository;
            _context = context;
            _userAgentAuthService = userAgentAuthService;
            _systemInfoService = systemInfoService;

            _actionBlock = new ActionBlock<SessionArg>(
                SessionChangeHandler,
                new ExecutionDataflowBlockOptions
                {
                    SingleProducerConstrained = true,
                    CancellationToken = cancellationToken
                });
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _subscription = _context.SessionChanges
                                    .Where(x => IsActivateSession(x) || IsDeactivateSession(x))
                                    .Subscribe(_actionBlock.AsObserver());
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _subscription?.Dispose();
            _actionBlock.Complete();
            return Task.CompletedTask;
        }
        
        [SuppressMessage("ReSharper", "PossibleInvalidOperationException")]
        private Task SessionChangeHandler(SessionArg arg)
        {
            var request = new LogonRequest(
                arg.User.UserName,
                arg.User.Domain,
                arg.User.IsLocalUser,
                _systemInfoService.GetDeviceGuid(),
                arg.User.DisplayName);

            if (IsActivateSession(arg))
            {
                return RegisterChangedSession(RegisterLogonSession, request, (SessionChangeReason)arg.Reason);
            }

            if (IsDeactivateSession(arg))
            {
                return RegisterChangedSession(_userAgentAuthService.LogOffAsync, request, (SessionChangeReason)arg.Reason);
            }

            return Task.CompletedTask;
        }

        private bool IsActivateSession(SessionArg arg)
            => arg.Reason.IsIn(
                SessionChangeReason.SessionLogon,
                SessionChangeReason.SessionUnlock);

        private bool IsDeactivateSession(SessionArg arg)
            => arg.Reason.IsIn(
                SessionChangeReason.SessionLogoff,
                SessionChangeReason.SessionLock);

        private async Task RegisterChangedSession<T>(Func<T, Task> registrarAction, T request, SessionChangeReason reason)
            where T : UserIdentity
        {
            var waitAndRetry = Policy
                               .Handle<Exception>()
                               .WaitAndRetryAsync(retryCount: 3, i => TimeSpan.FromSeconds(5));

            var fallback = Policy
                           .Handle<Exception>()
                           .FallbackAsync(x => SaveToLocalDB(request, reason));

            await Policy.WrapAsync(waitAndRetry, fallback)
                        .ExecuteAsync(() => registrarAction?.Invoke(request));
        }

        private async Task RegisterLogonSession(LogonRequest request)
        {
            var userAgentId = await _userAgentAuthService.LogOnAsync(request);
            if (!userAgentId.IsNullOrEmpty())
            {
                _repository.Upsert(new UserIdentifier(userAgentId, request.UserName));
            }
        }

        private async Task SaveToLocalDB(UserIdentity userIdentity, SessionChangeReason reason)
        {
            _repository.Insert(
                new UserSessionChange(
                    userIdentity.DeviceUUID,
                    userIdentity.Domain,
                    userIdentity.IsLocalUser,
                    userIdentity.UserName,
                    reason));
            await Task.CompletedTask;
        }
    }
}