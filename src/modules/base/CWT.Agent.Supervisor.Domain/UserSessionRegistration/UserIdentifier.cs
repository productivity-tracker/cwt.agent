﻿using System;
using LiteDB;

namespace CWT.Agent.Supervisor.Domain.UserSessionRegistration
{
    public class UserIdentifier : IEquatable<UserIdentifier>
    {
        [BsonId]
        public string UserName { get; private set; }

        public string RegisterId { get; private set; }

        protected UserIdentifier() { }

        public UserIdentifier(string registerId, string userName)
        {
            RegisterId = registerId;
            UserName = userName;
        }

        #region IEquatable Support

        public bool Equals(UserIdentifier other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return UserName == other.UserName
                && RegisterId == other.RegisterId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((UserIdentifier)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((UserName != null ? UserName.GetHashCode() : 0) * 397) ^ (RegisterId != null ? RegisterId.GetHashCode() : 0);
            }
        }

        public static bool operator ==(UserIdentifier left, UserIdentifier right) => Equals(left, right);

        public static bool operator !=(UserIdentifier left, UserIdentifier right) => !Equals(left, right);

        #endregion
    }
}