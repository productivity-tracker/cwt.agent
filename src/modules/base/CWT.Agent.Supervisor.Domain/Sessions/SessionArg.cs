﻿using System;
using CWT.Agent.PlatformInfo.Windows.System;

namespace CWT.Agent.Supervisor.Domain.Sessions
{
    public class SessionArg : IEquatable<SessionArg>
    {
        public uint SessionId { get; }
        public SessionChangeReason? Reason { get; }
        public UserInfo User { get; }

        public SessionArg(SessionChangeReason? reason, uint sessionId, UserInfo user)
        {
            Reason = reason;
            SessionId = sessionId;
            User = user;
        }

        #region IEquatable Support

        public bool Equals(SessionArg other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return SessionId == other.SessionId && Reason == other.Reason && Equals(User, other.User);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SessionArg)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)SessionId;
                hashCode = (hashCode * 397) ^ Reason.GetHashCode();
                hashCode = (hashCode * 397) ^ (User != null ? User.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(SessionArg left, SessionArg right) => Equals(left, right);

        public static bool operator !=(SessionArg left, SessionArg right) => !Equals(left, right);

        #endregion
    }
}