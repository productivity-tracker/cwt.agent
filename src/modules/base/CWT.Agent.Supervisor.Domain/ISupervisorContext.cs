﻿using System;
using CWT.Agent.Supervisor.Domain.Sessions;

namespace CWT.Agent.Supervisor.Domain
{
    public interface ISupervisorContext
    {
        event Action Started;

        IObservable<SessionArg> SessionChanges { get; }
    }
}