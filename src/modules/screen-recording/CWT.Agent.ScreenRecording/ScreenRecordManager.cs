﻿using System;
using CWT.Agent.Base;
using CWT.Agent.Grabber;
using CWT.Agent.ScreenRecording.Factories;
using LiteDB;
using ScreenRecording.Application.Contracts.RecordSettings.DTO;
using ScreenRecordingAdministration.ScreenRecordSettings;

namespace CWT.Agent.ScreenRecording
{
    public class ScreenRecordManager : IDisposable
    {
        private const string SETTINGS_COLLECTION = "ScreenRecordSettings";

        private readonly GrabberContext _context;
        private readonly ILiteRepository _settingsRepository;

        private readonly ScreenshotRecorderFactory _screenshotRecorderFactory;
        private readonly ScreenVideoRecorderFactory _screenVideoRecorderFactory;

        private readonly object _syncObj = new object();
        private readonly IDisposable _settingsSubscription;
        private IScreenRecorder _recorder;

        public ScreenRecordManager(
            ILiteRepository settingsRepository,
            GrabberContext context,
            ScreenshotRecorderFactory screenshotRecorderFactory,
            ScreenVideoRecorderFactory screenVideoRecorderFactory,
            IObservable<ScreenRecordAgentSettingsDto> settings)
        {
            _context = context;
            _screenshotRecorderFactory = screenshotRecorderFactory;
            _screenVideoRecorderFactory = screenVideoRecorderFactory;
            _settingsRepository = settingsRepository;
            _settingsSubscription = settings.Subscribe(OnSettingsChanged);
        }

        public void Start()
        {
            var settings = GetSettings(_context.UserAgentId);
            ReloadRecorder(settings);
        }

        public void Dispose()
        {
            _settingsSubscription?.Dispose();
            _recorder?.Stop();
        }

        private void OnSettingsChanged(ScreenRecordAgentSettingsDto settings)
        {
            var settingsHolder = new SettingsHolder<ScreenRecordAgentSettingsDto>(_context.UserAgentId, settings);
            _settingsRepository.Upsert(settingsHolder, SETTINGS_COLLECTION);

            ReloadRecorder(settings);
        }

        private void ReloadRecorder(ScreenRecordAgentSettingsDto settings)
        {
            lock (_syncObj)
            {
                _recorder?.Stop();
                if (!(settings?.EnableRecording ?? false)) return;

                var factory = SelectFactory(settings.RecordType);
                _recorder = factory.Create(settings);
                _recorder.Start();
            }
        }

        private IScreenRecorderFactory SelectFactory(RecordType recordType) => recordType switch
        {
            RecordType.Screenshot => (IScreenRecorderFactory)_screenshotRecorderFactory,
            RecordType.DesktopDuplicationStream => (IScreenRecorderFactory)_screenVideoRecorderFactory,
            RecordType.Auto => (IScreenRecorderFactory)_screenshotRecorderFactory,
            _ => throw new NotSupportedException(recordType.ToString("G"))
        };

        private ScreenRecordAgentSettingsDto GetSettings(string userAgentId)
        {
            var holder = _settingsRepository.FirstOrDefault<SettingsHolder<ScreenRecordAgentSettingsDto>>(
                x => x.UserAgentId == userAgentId, SETTINGS_COLLECTION);
            return holder?.Value ?? new ScreenRecordAgentSettingsDto
            {
                EnableRecording = false
            };
        }
    }
}