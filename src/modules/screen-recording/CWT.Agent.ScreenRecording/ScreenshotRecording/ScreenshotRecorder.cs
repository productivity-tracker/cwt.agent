﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using CWT.Agent.PlatformInfo.Windows;
using CWT.Agent.ScreenCapture.Capture;
using CWT.Agent.ScreenCapture.Images;
using CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace CWT.Agent.ScreenRecording.ScreenshotRecording
{
    internal class ScreenshotRecorder : IScreenRecorder
    {
        private readonly ActionBlock<ScreenRecordReason> _actionBlock;
        private readonly CancellationTokenSource _cts;
        private readonly ILogger _logger;

        private readonly IPlatformService _platformService;
        private readonly IScreenCaptureService _screenCaptureService;
        private readonly IScreenshotHandler _screenshotHandler;
        
        private readonly IObservable<ScreenRecordReason> _signals;
        private IDisposable _subscription;
        
        public ScreenshotRecorder(
            ILoggerFactory loggerFactory,
            IObservable<ScreenRecordReason> signals,
            IPlatformService platformService,
            IScreenCaptureService screenCaptureService,
            IScreenshotHandler handler)
        {
            _logger = loggerFactory?.CreateLogger<ScreenshotRecorder>() ?? NullLogger<ScreenshotRecorder>.Instance;
            _screenshotHandler = handler;
            _screenCaptureService = screenCaptureService;
            _platformService = platformService;
            _cts = new CancellationTokenSource();

            _actionBlock = new ActionBlock<ScreenRecordReason>(
                DoRecord,
                new ExecutionDataflowBlockOptions
                {
                    SingleProducerConstrained = true,
                    BoundedCapacity = 15,
                    MaxDegreeOfParallelism = 3,
                    CancellationToken = _cts.Token
                });

            _signals = signals;
        }

        public void Start()
        {
            _subscription = _signals.Subscribe(_actionBlock.AsObserver());
            _logger.LogInformation("Start recording.");
        }

        public void Stop()
        {
            _subscription?.Dispose();
            _cts.Cancel();
            _logger.LogInformation("Stop recording.");
        }

        private async Task DoRecord(ScreenRecordReason reason)
        {
            if (_screenshotHandler.CanContinue && !_cts.IsCancellationRequested)
            {
                IBitmapImage image = null;

                try
                {
                    image = _screenCaptureService.Capture(_platformService.DesktopRectangle, true);
                    await _screenshotHandler.Handle(image);
                }
                catch (Win32Exception ex)
                {
                    _logger.LogDebug(ex, ex.Message);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }
                finally
                {
                    image?.Dispose();
                }
            }
        }
    }
}