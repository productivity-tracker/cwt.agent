﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CWT.Agent.ScreenCapture.Images;

namespace CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers
{
    public class ScreenshotLocalStorekeeper : ImageConverter, IScreenshotHandler
    {
        public bool CanContinue => false;

        public ScreenshotLocalStorekeeper(ImageFormat imgFormat, long imgQuality)
            : base(imgFormat, imgQuality)
        {
        }

        public Task Handle(IBitmapImage image, CancellationToken cancellation)
        {
            var ms = new MemoryStream();

            try
            {
                //ConvertAndCompress(image, ms);
                return Task.CompletedTask;
            }
            finally
            {
                ms.Dispose();
            }
        }
    }
}