﻿using System.Threading;
using System.Threading.Tasks;
using CWT.Agent.ScreenCapture.Images;

namespace CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers
{
    public interface IScreenshotHandler
    {
        bool CanContinue { get; }

        Task Handle(IBitmapImage image, CancellationToken cancellation = default);
    }
}