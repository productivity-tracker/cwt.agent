﻿using System.IO;
using CWT.Agent.ScreenCapture.Images;

namespace CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers
{
    public class ImageConverter
    {
        protected ImageFormat ImgFormat { get; }
        protected long ImgQuality { get; }

        public ImageConverter(ImageFormat imgFormat = ImageFormat.Jpg, long imgQuality = 70L)
        {
            ImgFormat = imgFormat;
            ImgQuality = imgQuality;
        }

        protected void ConvertAndCompress(IBitmapImage image, Stream stream) 
            => image.Save(stream, ImgFormat, ImgQuality);
    }
}