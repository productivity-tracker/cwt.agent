﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CWT.Agent.Grabber;
using CWT.Agent.ScreenCapture.Images;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Polly;
using Polly.CircuitBreaker;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;

namespace CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers
{
    public class ScreenshotUploader : ImageConverter, IScreenshotHandler
    {
        private const string OPERATION_PATH = "/api/screenRecording/screenshots/";

        private readonly ILogger _logger;

        private readonly MediaTypeHeaderValue _mediaType;
        private readonly GrabberContext _context;

        private readonly string _destinationUrl;
        private readonly HttpClient _client;
        private readonly AsyncCircuitBreakerPolicy<HttpResponseMessage> _breakerPolicy;

        public bool CanContinue =>
            _breakerPolicy.CircuitState != CircuitState.Isolated
         && (_breakerPolicy.CircuitState == CircuitState.Closed
          || _breakerPolicy.CircuitState == CircuitState.HalfOpen);

        public ScreenshotUploader(
            ILoggerFactory loggerFactory,
            ImageFormat imgFormat,
            long imgQuality,
            GrabberContext context,
            string baseUrl)
            : base(imgFormat, imgQuality)
        {
            _logger = loggerFactory?.CreateLogger<ScreenshotUploader>() ?? NullLogger<ScreenshotUploader>.Instance;

            if (baseUrl.IsNullOrWhiteSpace()) throw new ArgumentException("The base url can not be null or empty.");

            _mediaType = TryGetMediaType(imgFormat);
            _destinationUrl = new StringBuilder().Append(baseUrl.TrimEnd('/'))
                                                 .Append($"/{OPERATION_PATH.Trim('/')}")
                                                 .ToString();
            _context = context;
            _client = new HttpClient();
            _breakerPolicy = CreatePolicy();
        }

        public async Task Handle(IBitmapImage image, CancellationToken cancellation)
        {
            var ms = new MemoryStream();

            try
            {
                ConvertAndCompress(image, ms);
                await _breakerPolicy.ExecuteAsync(
                    ct =>
                    {
                        using var multiContent = new MultipartFormDataContent();
                        using var imageContent = new ByteArrayContent(ms.ToArray());
                        imageContent.Headers.ContentType = _mediaType;
                        multiContent.Add(imageContent, nameof(ScreenshotDto.File), fileName: "screenshot");
                        multiContent.Add(new StringContent(_context.UserAgentId), nameof(ScreenshotDto.UserAgentId));
                        multiContent.Add(
                            new StringContent(new DateTimeOffset(image.CreatedAtUtc).ToUnixTimeSeconds().ToString()),
                            nameof(ScreenshotDto.Timestamp));

                        return _client.PostAsync(_destinationUrl, multiContent, ct);
                    },
                    cancellation);
            }
            finally
            {
                ms.Dispose();
            }
        }

        private AsyncCircuitBreakerPolicy<HttpResponseMessage> CreatePolicy()
            => Policy.HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                     .Or<Exception>()
                     .AdvancedCircuitBreakerAsync(
                         failureThreshold: 0.7,
                         samplingDuration: TimeSpan.FromSeconds(5),
                         minimumThroughput: 10,
                         durationOfBreak: TimeSpan.FromSeconds(30),
                         onBreak: async (
                             outcome,
                             state,
                             timeSpan,
                             context) =>
                         {
                             _logger.LogError($"The breaker tripped, circuit interrupted ({state.ToString()})");
                             if (outcome.Exception != null)
                             {
                                 _logger.LogError($"Error occured while upload screenshot. {outcome.Exception.Message}");
                             }

                             if (outcome.Result == null) return;

                             var response = await outcome.Result.Content.ReadAsStringAsync();
                             _logger
                                 .LogError($"Bad response while upload screenshot ({outcome.Result.StatusCode}). " + $"Message: {response}");
                         },
                         onReset: context => _logger.LogInformation("Destination storage restored, circuit resumed"),
                         onHalfOpen: () => { });

        private MediaTypeHeaderValue TryGetMediaType(ImageFormat imgFormat)
        {
            var contentType = imgFormat switch
            {
                ImageFormat.Jpg => "image/jpeg",
                ImageFormat.Png => "image/png",
                ImageFormat.Bmp => "image/bmp",
                _ => throw new ArgumentOutOfRangeException(imgFormat.ToString("G"))
            };

            return MediaTypeHeaderValue.TryParse(contentType, out var mediaType)
                ? mediaType
                : throw new ArgumentException($"Not found media type for {imgFormat:G}");
        }
    }
}