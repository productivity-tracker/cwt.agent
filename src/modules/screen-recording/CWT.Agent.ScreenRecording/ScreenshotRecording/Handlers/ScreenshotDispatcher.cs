﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CWT.Agent.ScreenCapture.Images;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Polly;

namespace CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers
{
    public class ScreenshotDispatcher : IScreenshotHandler
    {
        private readonly ILogger _logger;

        private readonly IScreenshotHandler _next;
        private readonly IScreenshotHandler _fallback;

        public bool CanContinue => _next.CanContinue || _fallback.CanContinue;

        public ScreenshotDispatcher(
            ILoggerFactory loggerFactory,
            IScreenshotHandler next,
            IScreenshotHandler fallback)
        {
            _logger = loggerFactory.CreateLogger<ScreenshotDispatcher>() ?? NullLogger<ScreenshotDispatcher>.Instance;

            _next = next ?? throw new ArgumentNullException(nameof(next));
            _fallback = fallback ?? throw new ArgumentNullException(nameof(fallback));
        }

        public Task Handle(IBitmapImage image, CancellationToken cancellation)
        {
            Task FallbackAction(CancellationToken ct)
            {
                if (_fallback.CanContinue) return _fallback.Handle(image, ct);

                _logger.LogDebug("There are no handlers available.");
                return Task.CompletedTask;
            }

            if (_next.CanContinue)
            {
                return Policy.Handle<Exception>()
                             .FallbackAsync(
                                 FallbackAction,
                                 ex =>
                                 {
                                     _logger.LogDebug(ex, ex.Message);
                                     return Task.CompletedTask;
                                 })
                             .ExecuteAsync(ct => _next.Handle(image, ct), cancellation);
            }

            return FallbackAction(cancellation);
        }
    }
}