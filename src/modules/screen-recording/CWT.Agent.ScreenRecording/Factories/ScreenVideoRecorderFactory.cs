﻿using CWT.Agent.ScreenRecording.ScreenVideoRecording;
using Microsoft.Extensions.Logging;
using ScreenRecording.Application.Contracts.RecordSettings.DTO;

namespace CWT.Agent.ScreenRecording.Factories
{
    public class ScreenVideoRecorderFactory : ScreenRecorderFactory
    {
        public ScreenVideoRecorderFactory(ILoggerFactory loggerFactory) 
            :base(loggerFactory)
        {
        }

        public override IScreenRecorder Create(ScreenRecordAgentSettingsDto settings) 
            => new ScreenVideoRecorderMock();
    }
}
