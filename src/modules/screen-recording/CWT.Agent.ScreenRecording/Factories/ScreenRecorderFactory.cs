﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using CWT.Agent.Base.Extensions;
using CWT.Agent.ProcessMonitoring;
using CWT.Agent.SiteTracking;
using Microsoft.Extensions.Logging;
using ScreenRecording.Application.Contracts.RecordSettings.DTO;
using ScreenRecordingAdministration.ScreenRecordSettings;

namespace CWT.Agent.ScreenRecording.Factories
{
    public abstract class ScreenRecorderFactory : IScreenRecorderFactory
    {
        protected ILoggerFactory LoggerFactory { get; }

        protected ScreenRecorderFactory(ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
        }

        public abstract IScreenRecorder Create(ScreenRecordAgentSettingsDto settings);

        protected IObservable<ScreenRecordReason> CreateSignal(ScreenRecordAgentSettingsDto settings)
        {
            if (settings == null || !settings.EnableRecording) 
                return Observable.Empty<ScreenRecordReason>();

            var signals = new List<IObservable<ScreenRecordReason>>();
            var mode = settings.RecordMode;
            var captureInterval = settings.FrameFrequency;
            if (mode.HasFlag(RecordMode.WebSiteMonitoring))
            {
                var browserSpy = new BrowserSpy(LoggerFactory, TimeSpan.FromSeconds(2)); // TODO Must be disposable
                signals.Add(browserSpy.ActiveUrls
                                       .Where(url => 
                                                  settings.ObservableWebSites.Any(p => Regex.IsMatch(url, p)))
                                       .Select(_ => ScreenRecordReason.VisitWebSite));
            }

            if (mode.HasFlag(RecordMode.Always))
            {
                signals.Add(Observable.Interval(captureInterval)
                                       .Select(_ => ScreenRecordReason.RegularTimer));
            }

            if (mode.HasFlag(RecordMode.ProcessMonitoring))
            {
                var processMonitor = new MultiProcessMonitor(LoggerFactory, settings.ObservableProcesses); // TODO
                signals.Add(processMonitor.StateChanges
                                          .Scan(new HashSet<string>(),
                                                (activeProcesses, newChange) =>
                                                {
                                                    switch (newChange.Reason)
                                                    {
                                                        case ProcessStateChangedReason.Started:
                                                            activeProcesses.AddIfNotContains(newChange.Name);
                                                            break;
                                                        case ProcessStateChangedReason.Terminated:
                                                            activeProcesses.Remove(newChange.Name);
                                                            break;
                                                        default:
                                                            throw new ArgumentOutOfRangeException();
                                                    }

                                                    return activeProcesses;
                                                })
                                          .Where(activeProcesses => activeProcesses.Any())
                                          .RepeatLastValueDuringSilence(captureInterval)
                                          .Select(_ => ScreenRecordReason.ProcessActive));
            }

            return signals.Count > 1 
                ? signals.Merge() 
                : signals.FirstOrDefault() ?? Observable.Empty<ScreenRecordReason>();
        }
    }
}
