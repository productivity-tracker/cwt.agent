﻿using System;
using System.Linq;
using System.Reactive.Linq;
using CWT.Agent.Grabber;
using CWT.Agent.PlatformInfo.Windows;
using CWT.Agent.ScreenCapture.Capture;
using CWT.Agent.ScreenCapture.Images;
using CWT.Agent.ScreenRecording.ScreenshotRecording;
using CWT.Agent.ScreenRecording.ScreenshotRecording.Handlers;
using Microsoft.Extensions.Logging;
using ScreenRecording.Application.Contracts.RecordSettings.DTO;
using ExternalImageFormat = ScreenRecording.Domain.Shared.ImageFormat;

namespace CWT.Agent.ScreenRecording.Factories
{
    public class ScreenshotRecorderFactory : ScreenRecorderFactory
    {
        private const int MIN_INTERVAL_BETWEEN_SIGNALS_IN_MILLISECONDS = 1000;
        private readonly IPlatformService _platformService;
        private readonly IScreenCaptureService _screenCaptureService;
        private readonly GrabberContext _context;

        public ScreenshotRecorderFactory(
            ILoggerFactory loggerFactory,
            IPlatformService platformService,
            IScreenCaptureService screenCaptureService,
            GrabberContext context)
        : base(loggerFactory)
        {
            _platformService = platformService;
            _screenCaptureService = screenCaptureService;
            _context = context;
        }

        public override IScreenRecorder Create(ScreenRecordAgentSettingsDto settings)
        {
            var signals = CreateSignal(settings)
                          .Buffer(TimeSpan.FromMilliseconds(MIN_INTERVAL_BETWEEN_SIGNALS_IN_MILLISECONDS))
                          .Where(s => s.Any())
                          .Select(s => s.Last());

            return new ScreenshotRecorder(
                LoggerFactory,
                signals,
                _platformService,
                _screenCaptureService,
                CreateChainHandlers(settings));
        }

        private IScreenshotHandler CreateChainHandlers(ScreenRecordAgentSettingsDto settings)
        {
            var format = Translate(settings.ScreenshotSettings.ImageFormat);
            var quality = settings.ScreenshotSettings.Quality;
            var uploader = new ScreenshotUploader(LoggerFactory, format, quality, _context, settings.StorageAdapterAddress);
            var localStorekeeper = new ScreenshotLocalStorekeeper(format, quality);
            return new ScreenshotDispatcher(LoggerFactory, uploader, localStorekeeper);
        }

        private ImageFormat Translate(ExternalImageFormat format) => format switch
        {
            ExternalImageFormat.Jpg => ImageFormat.Jpg,
            ExternalImageFormat.Png => ImageFormat.Png,
            ExternalImageFormat.Bmp => ImageFormat.Bmp,
            _ => throw new ArgumentOutOfRangeException(format.ToString("G"))
        };
    }
}
