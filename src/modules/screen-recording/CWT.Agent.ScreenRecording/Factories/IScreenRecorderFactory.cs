﻿using ScreenRecording.Application.Contracts.RecordSettings.DTO;

namespace CWT.Agent.ScreenRecording.Factories
{
    internal interface IScreenRecorderFactory
    {
        IScreenRecorder Create(ScreenRecordAgentSettingsDto settings);
    }
}
