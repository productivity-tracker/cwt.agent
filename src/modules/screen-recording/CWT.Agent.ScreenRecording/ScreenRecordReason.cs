﻿namespace CWT.Agent.ScreenRecording
{
    public enum ScreenRecordReason
    {
        RegularTimer,
        VisitWebSite,
        ProcessActive,
        UserAction
    }
}
