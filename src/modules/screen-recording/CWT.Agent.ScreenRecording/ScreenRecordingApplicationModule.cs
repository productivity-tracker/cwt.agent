﻿using System;
using System.IO;
using Autofac;
using CWT.Agent.ScreenRecording.Factories;
using LiteDB;

namespace CWT.Agent.ScreenRecording
{
    public class ScreenRecordingApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ScreenshotRecorderFactory>().SingleInstance();
            builder.RegisterType<ScreenVideoRecorderFactory>().SingleInstance();
            builder.RegisterType<ScreenRecordManager>().SingleInstance();
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            builder.RegisterInstance(
                new LiteRepository($@"Filename={Path.Combine(appData, @"cwt\grabber.db")};"+
                                   @"Password=gd2uDWwg0QDxd885Ed:5NH0sXf?u0$J7?=DSY6o}B)jpffWP{d"))
                   .As<ILiteRepository>().SingleInstance();
        }
    }
}