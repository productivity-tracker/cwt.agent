﻿namespace CWT.Agent.ScreenRecording
{
    public interface IScreenRecorder
    {
        void Start();
        void Stop();
    }
}
