﻿using System;
using System.Diagnostics;
using System.Windows.Automation;
using UIAutomationClient;
using SystemTreeScope = System.Windows.Automation.TreeScope;
using TreeScope = UIAutomationClient.TreeScope;

namespace CWT.Agent.SiteTracking
{
    public static class BrowserUrlProvider
    {
        public static string GetChromeUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException(nameof(process));

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            var element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            var elm = element.FindFirst(SystemTreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));

            return elm == null ? null : ((ValuePattern)elm.GetCurrentPattern(ValuePattern.Pattern)).Current.Value;
        }

        public static string GetChromeUrlV2(Process process)
        {
            var _automation = new CUIAutomation();
            var appElement = _automation.ElementFromHandle(process.MainWindowHandle);

            var conditionArray = new IUIAutomationCondition[3];
            conditionArray[0] = _automation.CreatePropertyCondition(UIA_PropertyIds.UIA_ProcessIdPropertyId, process.Id);
            conditionArray[1] = _automation.CreatePropertyCondition(UIA_PropertyIds.UIA_NamePropertyId, "Google Chrome");
            conditionArray[2] = _automation.CreatePropertyCondition(
                UIA_PropertyIds.UIA_ControlTypePropertyId,
                UIA_ControlTypeIds.UIA_PaneControlTypeId);
            var conditions = _automation.CreateAndConditionFromArray(conditionArray);
            var frameElement = appElement.FindFirst(TreeScope.TreeScope_Subtree, conditions);
            if (frameElement != null)
            {
                conditionArray = new IUIAutomationCondition[2];
                conditionArray[0] = _automation.CreatePropertyCondition(UIA_PropertyIds.UIA_ProcessIdPropertyId, process.Id);
                conditionArray[1] = _automation.CreatePropertyCondition(
                    UIA_PropertyIds.UIA_ControlTypePropertyId,
                    UIA_ControlTypeIds.UIA_EditControlTypeId);
                conditions = _automation.CreateAndConditionFromArray(conditionArray);
                frameElement = frameElement.FindFirst(TreeScope.TreeScope_Descendants, conditions);
                return ((IUIAutomationValuePattern)frameElement?.GetCurrentPattern(10002))?.CurrentValue;
            }

            return null;
        }

        public static string GetInternetExplorerUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            var element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            var rebar = element.FindFirst(SystemTreeScope.Children, new PropertyCondition(AutomationElement.ClassNameProperty, "ReBarWindow32"));
            if (rebar == null)
                return null;

            var edit = rebar.FindFirst(SystemTreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));

            return ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value;
        }

        public static string GetFirefoxUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            var element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            var doc = element.FindFirst(SystemTreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Document));
            if (doc == null)
                return null;

            return ((ValuePattern)doc.GetCurrentPattern(ValuePattern.Pattern)).Current.Value;
        }
    }
}