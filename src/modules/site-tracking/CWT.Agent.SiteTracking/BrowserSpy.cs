﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text.RegularExpressions;
using CWT.Agent.Windows.Shared.Native;
using CWT.Agent.Windows.Shared.WindowTracking;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace CWT.Agent.SiteTracking
{
    public class BrowserSpy : IDisposable
    {
        private readonly ILogger _logger;
        private readonly ISet<string> _knownBrowsers = new HashSet<string>
        {
            "chrome"
            //"firefox",
            //"iexplore"
        };

        private readonly EventHook _eventHook;

        private readonly ISubject<Process> _browsersSubject;
        public IObservable<string> ActiveUrls;

        public BrowserSpy(ILoggerFactory loggerFactory, TimeSpan interval)
        {
            _logger = loggerFactory?.CreateLogger<BrowserSpy>() ?? NullLogger<BrowserSpy>.Instance;
            _eventHook = new EventHook(OnForegroundChanged, EventHook.EVENT_SYSTEM_FOREGROUND);
            _browsersSubject = new Subject<Process>();
            ActiveUrls = _browsersSubject.Where(p => _knownBrowsers.Contains(p.ProcessName))
                                         .Select(
                                             x => Observable.Interval(interval)
                                                            .TakeUntil(_browsersSubject)
                                                            .Select(_ => TryGetUrl(x))
                                                            .Retry()
                                                            .Where(IsValidUrl))
                                         .Switch();
        }

        public void Dispose() => _eventHook.Stop();

        private string TryGetUrl(Process process)
        {
            try
            {
                return BrowserUrlProvider.GetChromeUrl(process);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred while grab url from browser {process.ProcessName}. ", ex);
                return null;
            }
        }

        private void OnForegroundChanged(
            IntPtr hWinEventHook,
            uint eventType,
            IntPtr hWnd,
            int idObject,
            int idChild,
            uint dwEventThread,
            uint dwmsEventTime)
        {
            User32.GetWindowThreadProcessId(hWnd, out var processId);
            if (processId == 0)
            {
                return;
            }

            var foregroundProcess = Process.GetProcessById(processId);
            _browsersSubject.OnNext(foregroundProcess);
        }

        private bool IsValidUrl(string urlString) =>
            !string.IsNullOrWhiteSpace(urlString)
         && Regex.IsMatch(
                urlString,
                @"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$");
    }
}