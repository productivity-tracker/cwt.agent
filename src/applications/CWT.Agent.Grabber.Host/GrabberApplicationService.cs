﻿using CWT.Agent.ScreenRecording;

namespace CWT.Agent.Grabber.Host
{
    public class GrabberApplicationService
    {
        private readonly IScreenRecorder _recorder;

        public GrabberApplicationService(IScreenRecorder recorder)
        {
            _recorder = recorder;
        }

        //public async Task RunAsync()
        //{

        //}

        public void Run()
        {
            _recorder.Start();
        }
    }
}