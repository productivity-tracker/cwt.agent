﻿using System;
using System.CommandLine;
using System.CommandLine.DragonFruit;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Fluent;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using NullLogger = Microsoft.Extensions.Logging.Abstractions.NullLogger;
using Process = System.Diagnostics.Process;

namespace CWT.Agent.Grabber.Host
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string UNIQUE_APP_ID = "05fedf15-1da7-43ed-82d7-e9f263905328";

        private Mutex _appMutex;
        private ILogger _logger;

        public App()
        {
            _logger = NullLogger.Instance;

#if !DEBUG
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            //WinApplication.Current.DispatcherUnhandledException += DispatcherOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
#endif
            // Set to use only second processor core
            Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(2);

            SingleInstanceCheck();
        }
        
        protected override void OnStartup(StartupEventArgs e)
        {
            var cmdHandleMethod = typeof(App).GetMethod(nameof(InputArgsHandler));
            var rootCommand = new RootCommand();
            rootCommand.ConfigureFromMethod(cmdHandleMethod);
            rootCommand.InvokeAsync(e.Args).Wait();

            ILifetimeScope scope;
            try
            {
                var container = ConfigureContainer();
                scope = container.BeginLifetimeScope();

                _logger = scope.Resolve<ILoggerFactory>().CreateLogger(nameof(App));
                var grabber = scope.Resolve<GrabberApplicationService>();
                grabber.Run();

                _logger.LogInformation($"Started {nameof(GrabberApplicationService)}");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occured while {nameof(OnStartup)}");
                throw;
            }

            Current.Exit += (s, a) =>
            {
                scope?.Dispose();
                _logger.LogInformation($"Closing {nameof(GrabberApplicationService)}");
                LogManager.Flush();
            };

            base.OnStartup(e);
        }

        private void InputArgsHandler(string userAgentId)
        {
            // TODO: Set userAgentId to GrabberContext
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs args)
        {
            _logger.LogCritical("Unhandled task exception");
            args.SetObserved();

            HandleException(args.Exception.GetBaseException());
        }

        private void DispatcherOnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
        {
            _logger.LogCritical("Unhandled dispatcher thread exception");
            args.Handled = true;

            HandleException(args.Exception);
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Log.Fatal("Unhandled app domain exception");
            HandleException(args.ExceptionObject as Exception);
        }

        private void HandleException(Exception exception)
        {
            _logger.LogCritical("Exception", exception);
        }

        private IContainer ConfigureContainer()
        {
            var container = new ContainerBuilder();
            container.RegisterModule<GrabberHostModule>();
            return container.Build();
        }

        private void SingleInstanceCheck()
        {
            _appMutex = new Mutex(true, UNIQUE_APP_ID, out var canCreateNewInstance);
            if (canCreateNewInstance) return;

            _logger.LogWarning("Already running one instance of the application");
            Environment.Exit(0);
        }
    }
}