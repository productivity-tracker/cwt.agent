﻿using Autofac;
using CWT.Agent.Grabber.Host.Logging;
using CWT.Agent.PlatformInfo.Windows;
using CWT.Agent.ScreenCapture;
using CWT.Agent.ScreenRecording;

namespace CWT.Agent.Grabber.Host
{
    public class GrabberHostModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<NLoggerModule>();
            builder.RegisterModule<WindowsPlatformModule>();
            builder.RegisterModule<ScreenCaptureWindowsModule>();
            builder.RegisterModule<ScreenRecordingApplicationModule>();
            builder.RegisterType<GrabberApplicationService>().AsSelf();
        }
    }
}