﻿using System;
using System.IO;
using System.Reactive.Subjects;
using System.Threading;
using CWT.Agent.PlatformInfo.Windows.System;
using CWT.Agent.Supervisor.Domain;
using CWT.Agent.Supervisor.Domain.Sessions;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using Topshelf;
using Topshelf.Squirrel.Windows.Interfaces;
using Volo.Abp;

namespace CWT.Agent.Supervisor.Service
{
    public class ServiceImplementation : ISelfUpdatableService, ISupervisorContext
    {
        private readonly IAbpApplicationWithInternalServiceProvider _application;

        private readonly ISystemInfoService _systemInfoService;

        private readonly ISubject<SessionArg> _sessionChangedSubj;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly CancellationTokenSource _cts;

        public event Action Started;

        public IObservable<SessionArg> SessionChanges => _sessionChangedSubj;

        public ServiceImplementation()
        {
            _sessionChangedSubj = new Subject<SessionArg>();
            _cts = new CancellationTokenSource();
            _application = AbpApplicationFactory.Create<SupervisorApplicationModule>(
                options =>
                {
                    options.UseAutofac();
                    options.Services.AddUserRegistrar(this, _cts.Token);
                    options.Services.AddDeviceRegistrar(this, "https://localhost:5010/agents/connection", _cts.Token);
                    var grabberPath = Path.Combine(
                        AppDomain.CurrentDomain.BaseDirectory, 
                        System.Configuration.ConfigurationManager.AppSettings.Get("GrabberPath"));
                    options.Services.AddGrabberManager(this, grabberPath, _cts.Token);
                });
            _application.Initialize();

            _systemInfoService = _application.ServiceProvider.GetRequiredService<ISystemInfoService>();
        }

        public void Start()
        {
            _sessionChangedSubj.OnNext(GetInitialSessionArg());
            Started?.Invoke();
        }

        public void Stop()
        {
            _cts.Cancel();
            _application.Shutdown();
            _application?.Dispose();
        }
        
        public void OnSessionChange(SessionChangedArguments args)
            => _sessionChangedSubj.OnNext(new SessionArg(Translate(args.ReasonCode), (uint)args.SessionId, _systemInfoService.GetUserInfo()));

        private SessionChangeReason? Translate(SessionChangeReasonCode code)
        {
            switch (code)
            {
                case SessionChangeReasonCode.ConsoleConnect:
                    return SessionChangeReason.ConsoleConnect;
                case SessionChangeReasonCode.ConsoleDisconnect:
                    return SessionChangeReason.ConsoleDisconnect;
                case SessionChangeReasonCode.RemoteConnect:
                    return SessionChangeReason.RemoteConnect;
                case SessionChangeReasonCode.RemoteDisconnect:
                    return SessionChangeReason.RemoteDisconnect;
                case SessionChangeReasonCode.SessionLogon:
                    return SessionChangeReason.SessionLogon;
                case SessionChangeReasonCode.SessionLogoff:
                    return SessionChangeReason.SessionLogoff;
                case SessionChangeReasonCode.SessionLock:
                    return SessionChangeReason.SessionLock;
                case SessionChangeReasonCode.SessionUnlock:
                    return SessionChangeReason.SessionUnlock;
                case SessionChangeReasonCode.SessionRemoteControl:
                    return SessionChangeReason.SessionRemoteControl;
                default:
                    _logger.Warn($"Could not to resolve the reason of the session change: {code:G}");
                    return null;
            }
        }

        private SessionArg GetInitialSessionArg()
        {
            var activeSessionId = SessionInfo.WTSGetActiveConsoleSessionId();
            var session = SessionInfo.GetSession(activeSessionId);
            var user = new UserInfo(session.UserName, session.Domain, string.Empty);

            SessionChangeReason? reason = null;
            if (session.IsActive)
            {
                reason = session.State == SessionInfo.LockState.Unlocked 
                    ? SessionChangeReason.SessionUnlock 
                    : SessionChangeReason.SessionLock;
            }

            return new SessionArg(reason, activeSessionId, user);
        }
    }
}