﻿using System.Threading;
using AgentIdentity.Application.Contracts.Devices.Identity;
using AgentIdentity.Application.Contracts.UserAgents.Auth;
using CWT.Agent.PlatformInfo.Windows.System;
using CWT.Agent.Supervisor.Application;
using CWT.Agent.Supervisor.Domain;
using CWT.Agent.Supervisor.Domain.DeviceRegistration;
using CWT.Agent.Supervisor.Domain.UserSessionRegistration;
using LiteDB;
using Microsoft.Extensions.DependencyInjection;

namespace CWT.Agent.Supervisor.Service
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddUserRegistrar(
            this IServiceCollection @this,
            ISupervisorContext context,
            CancellationToken cancellationToken) =>
            @this.AddHostedService(
                provider =>
                {
                    var repository = provider.GetRequiredService<ILiteRepository>();
                    var userAgentAuthService = provider.GetRequiredService<IUserAgentAuthService>();
                    var systemInfoService = provider.GetRequiredService<ISystemInfoService>();
                    return new UserRegistrar(repository, context, userAgentAuthService, systemInfoService, cancellationToken);
                });

        public static IServiceCollection AddDeviceRegistrar(
            this IServiceCollection @this,
            ISupervisorContext context,
            string hubUrl,
            CancellationToken cancellationToken)
            => @this.AddHostedService(
                provider =>
                {
                    var deviceIdentityService = provider.GetRequiredService<IDeviceIdentityService>();
                    var systemInfoService = provider.GetRequiredService<ISystemInfoService>();
                    return new DeviceRegistrar(context, deviceIdentityService, systemInfoService, hubUrl, cancellationToken);
                });

        public static IServiceCollection AddGrabberManager(
            this IServiceCollection @this,
            ISupervisorContext context,
            string applicationPath,
            CancellationToken cancellation)
            => @this.AddHostedService(
                provider =>
                {
                    var repository = provider.GetRequiredService<ILiteRepository>();
                    return new GrabberManager(context, applicationPath, repository, cancellation);
                });
    }
}