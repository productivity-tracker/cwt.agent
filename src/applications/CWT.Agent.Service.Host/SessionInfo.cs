﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CWT.Agent.Supervisor.Service
{
    public static class SessionInfo
    {
        public class Session
        {
            public string UserName { get; set; }
            public string Domain { get; set; }
            public uint SessionId { get; set; }
            public string Client { get; set; }
            public string Server { get; set; }
            public WTS_CONNECTSTATE_CLASS ConnectionState { get; set; }
            public WTSINFOA Detail { get; set; }

            public bool IsActive => ConnectionState == WTS_CONNECTSTATE_CLASS.WTSActive;

            public LockState State => GetSessionLockState(SessionId);

            public override string ToString() => $"{SessionId};\t{string.Join("\\", Domain, UserName)}\t{ConnectionState}\t{Server}";
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct WTSINFOA
        {
            public const int WINSTATIONNAME_LENGTH = 32;
            public const int DOMAIN_LENGTH = 17;
            public const int USERNAME_LENGTH = 20;

            public WTS_CONNECTSTATE_CLASS State;
            public int SessionId;
            public int IncomingBytes;
            public int OutgoingBytes;
            public int IncomingFrames;
            public int OutgoingFrames;
            public int IncomingCompressedBytes;
            public int OutgoingCompressedBytes;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = WINSTATIONNAME_LENGTH)]
            public byte[] WinStationNameRaw;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = DOMAIN_LENGTH)]
            public byte[] DomainRaw;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = USERNAME_LENGTH + 1)]
            public byte[] UserNameRaw;

            public long ConnectTimeUTC;
            public long DisconnectTimeUTC;
            public long LastInputTimeUTC;
            public long LogonTimeUTC;
            public long CurrentTimeUTC;

            public string WinStationName => Normalize(WinStationNameRaw);

            public string Domain => Normalize(DomainRaw);

            public string UserName => Normalize(UserNameRaw);

            public DateTime ConnectTime => DateTime.FromFileTimeUtc(ConnectTimeUTC);

            public DateTime DisconnectTime => DateTime.FromFileTimeUtc(DisconnectTimeUTC);

            public DateTime LastInputTime => DateTime.FromFileTimeUtc(LastInputTimeUTC);

            public DateTime LogonTime => DateTime.FromFileTimeUtc(LogonTimeUTC);

            public DateTime CurrentTime => DateTime.FromFileTimeUtc(CurrentTimeUTC);

            private string Normalize(byte[] raw) => Encoding.ASCII.GetString(raw).Trim('\0');
        }

        private enum WTS_INFO_CLASS
        {
            WTSInitialProgram = 0,
            WTSApplicationName = 1,
            WTSWorkingDirectory = 2,
            WTSOEMId = 3,
            WTSSessionId = 4,
            WTSUserName = 5,
            WTSWinStationName = 6,
            WTSDomainName = 7,
            WTSConnectState = 8,
            WTSClientBuildNumber = 9,
            WTSClientName = 10,
            WTSClientDirectory = 11,
            WTSClientProductId = 12,
            WTSClientHardwareId = 13,
            WTSClientAddress = 14,
            WTSClientDisplay = 15,
            WTSClientProtocolType = 16,
            WTSIdleTime = 17,
            WTSLogonTime = 18,
            WTSIncomingBytes = 19,
            WTSOutgoingBytes = 20,
            WTSIncomingFrames = 21,
            WTSOutgoingFrames = 22,
            WTSClientInfo = 23,
            WTSSessionInfo = 24,
            WTSSessionInfoEx = 25,
            WTSConfigInfo = 26,
            WTSValidationInfo = 27,
            WTSSessionAddressV4 = 28,
            WTSIsRemoteSession = 29
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WTS_SESSION_INFO
        {
            public readonly uint SessionID;

            [MarshalAs(UnmanagedType.LPStr)]
            public readonly string pWinStationName;

            public readonly WTS_CONNECTSTATE_CLASS State;
        }

        private enum WTS_TYPE_CLASS
        {
            WTSTypeProcessInfoLevel0,
            WTSTypeProcessInfoLevel1,
            WTSTypeSessionInfoLevel1
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WTSINFOEX
        {
            public UInt32 Level;
            public UInt32 Reserved; /* I have observed the Data field is pushed down by 4 bytes so i have added this field as padding. */
            public WTSINFOEX_LEVEL Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WTSINFOEX_LEVEL
        {
            public WTSINFOEX_LEVEL1 WTSInfoExLevel1;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WTSINFOEX_LEVEL1
        {
            public UInt32 SessionId;
            public WTS_CONNECTSTATE_CLASS SessionState;
            public Int32 SessionFlags;

            /* I can't figure out what the rest of the struct should look like but as i don't need anything past the SessionFlags i'm not going to. */
        }

        public enum LockState
        {
            Unknown,
            Locked,
            Unlocked
        }

        public enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        private const int FALSE = 0;

        private const int WTS_SESSION_STATE_LOCK = 0;
        private const int WTS_SESSION_STATE_UNLOCK = 1;

        private static readonly IntPtr WTS_CURRENT_SERVER = IntPtr.Zero;

        private static readonly bool _isWin7;

        static SessionInfo()
        {
            var os_version = Environment.OSVersion;
            _isWin7 = os_version.Platform == PlatformID.Win32NT && os_version.Version.Major == 6 && os_version.Version.Minor == 1;
        }

        public static LockState GetSessionLockState(uint sessionId)
        {
            var result = WTSQuerySessionInformation(
                WTS_CURRENT_SERVER,
                sessionId,
                WTS_INFO_CLASS.WTSSessionInfoEx,
                out var ppBuffer,
                out var pBytesReturned
                );

            if (result == FALSE)
                return LockState.Unknown;

            var session_info_ex = Marshal.PtrToStructure<WTSINFOEX>(ppBuffer);

            if (session_info_ex.Level != 1)
                return LockState.Unknown;

            var lockState = session_info_ex.Data.WTSInfoExLevel1.SessionFlags;
            WTSFreeMemoryEx(WTS_TYPE_CLASS.WTSTypeSessionInfoLevel1, ppBuffer, pBytesReturned);

            if (_isWin7)
            {
                /* Ref: https://msdn.microsoft.com/en-us/library/windows/desktop/ee621019(v=vs.85).aspx
                    * Windows Server 2008 R2 and Windows 7:  Due to a code defect, the usage of the WTS_SESSIONSTATE_LOCK
                    * and WTS_SESSIONSTATE_UNLOCK flags is reversed. That is, WTS_SESSIONSTATE_LOCK indicates that the
                    * session is unlocked, and WTS_SESSIONSTATE_UNLOCK indicates the session is locked.
                    * */
                switch (lockState)
                {
                    case WTS_SESSION_STATE_LOCK:
                        return LockState.Unlocked;

                    case WTS_SESSION_STATE_UNLOCK:
                        return LockState.Locked;

                    default:
                        return LockState.Unknown;
                }
            }

            switch (lockState)
            {
                case WTS_SESSION_STATE_LOCK:
                    return LockState.Locked;

                case WTS_SESSION_STATE_UNLOCK:
                    return LockState.Unlocked;

                default:
                    return LockState.Unknown;
            }
        }

        [DllImport("kernel32.dll")]
        public static extern uint WTSGetActiveConsoleSessionId();

        public static bool IsActive(uint sessionID) => GetSession(sessionID)?.IsActive ?? false;

        public static bool IsActiveUser(string userName) => GetSession(userName)?.IsActive ?? false;

        public static Session GetSession(uint sessionID)
        {
            var sessions = GetUserList();
            return sessions.FirstOrDefault(x => x.SessionId == sessionID);
        }

        public static Session GetSession(string userName)
        {
            var sessions = GetUserList();
            return sessions.FirstOrDefault(x => string.Equals(x.UserName.Trim(), userName.Trim(), StringComparison.OrdinalIgnoreCase));
        }

        public static IList<Session> GetUserList(string serverName = "")
        {
            var sessions = new List<Session>();
            var serverHandle = WTSOpenServer(serverName);
            var sessionInfoPtr = IntPtr.Zero;

            try
            {
                var sessionCount = 0;
                var retVal = WTSEnumerateSessions(serverHandle, Reserved: 0, Version: 1, ref sessionInfoPtr, ref sessionCount);
                var dataSize = Marshal.SizeOf(typeof(WTS_SESSION_INFO));
                var currentSession = sessionInfoPtr;

                if (retVal != 0)
                {
                    for (var i = 0; i < sessionCount; i++)
                    {
                        var si = (WTS_SESSION_INFO)Marshal.PtrToStructure(currentSession, typeof(WTS_SESSION_INFO));
                        currentSession += dataSize;

                        WTSQuerySessionInformation(serverHandle, si.SessionID, WTS_INFO_CLASS.WTSClientName, out var clientNamePtr, out _);
                        WTSQuerySessionInformation(serverHandle, si.SessionID, WTS_INFO_CLASS.WTSSessionInfo, out var wtsInfoPtr, out _);

                        var wtsInfo = (WTSINFOA)Marshal.PtrToStructure(wtsInfoPtr, typeof(WTSINFOA));
                        var session = new Session
                        {
                            Client = Marshal.PtrToStringAnsi(clientNamePtr),
                            Server = serverName,
                            UserName = wtsInfo.UserName,
                            Domain = wtsInfo.Domain,
                            ConnectionState = si.State,
                            SessionId = si.SessionID,
                            Detail = wtsInfo
                        };
                        sessions.Add(session);

                        WTSFreeMemory(clientNamePtr);
                        WTSFreeMemory(wtsInfoPtr);
                    }

                    WTSFreeMemory(sessionInfoPtr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex); // TODO
                throw;
            }
            finally
            {
                WTSCloseServer(serverHandle);
            }

            return sessions;
        }

        [DllImport("wtsapi32.dll")]
        private static extern Int32 WTSQuerySessionInformation(
            IntPtr hServer,
            [MarshalAs(UnmanagedType.U4)] UInt32 SessionId,
            [MarshalAs(UnmanagedType.U4)] WTS_INFO_CLASS WTSInfoClass,
            out IntPtr ppBuffer,
            [MarshalAs(UnmanagedType.U4)] out UInt32 pBytesReturned
            );

        [DllImport("wtsapi32.dll")]
        private static extern void WTSFreeMemoryEx(WTS_TYPE_CLASS WTSTypeClass, IntPtr pMemory, uint NumberOfEntries);

        [DllImport("wtsapi32.dll")]
        private static extern IntPtr WTSOpenServer([MarshalAs(UnmanagedType.LPStr)] string pServerName);

        [DllImport("wtsapi32.dll")]
        private static extern void WTSCloseServer(IntPtr hServer);

        [DllImport("wtsapi32.dll")]
        private static extern int WTSEnumerateSessions(IntPtr hServer,
                                                       [MarshalAs(UnmanagedType.U4)] int Reserved,
                                                       [MarshalAs(UnmanagedType.U4)] int Version,
                                                       ref IntPtr ppSessionInfo,
                                                       [MarshalAs(UnmanagedType.U4)] ref int pCount);

        [DllImport("wtsapi32.dll")]
        private static extern void WTSFreeMemory(IntPtr pMemory);
    }
}