﻿using System;
using System.Threading;
using AgentIdentity.HttpApi.Client;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using CWT.Agent.PlatformInfo.Windows;
using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.Autofac;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace CWT.Agent.Supervisor.Service
{
    [DependsOn(typeof(AbpAutofacModule), typeof(AgentIdentityClientAppModule))]
    public class SupervisorApplicationModule : AbpModule, IDisposable
    {
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAutofac(builder => builder.RegisterModule<WindowsPlatformModule>());
            context.Services.AddSingleton<ILiteRepository>(
                new LiteRepository("Filename=supervisor.db; Timeout=00:01:00"));
            context.Services.AddLogging(builder => builder.AddNLog());
            context.Services.Configure<AbpRemoteServiceOptions>(options =>
            {
                options.RemoteServices.Default =
                    new RemoteServiceConfiguration("https://localhost:5010/");
            });
        }

        public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
        {
            var hostedServices = context.ServiceProvider.GetServices<IHostedService>();
            foreach (var hostedService in hostedServices)
            {
                hostedService.StartAsync(_cts.Token);
            }
        }

        public override void OnApplicationShutdown(ApplicationShutdownContext context)
        {
            var hostedServices = context.ServiceProvider.GetServices<IHostedService>();
            foreach (var hostedService in hostedServices)
            {
                hostedService.StopAsync(_cts.Token);
            }
        }

        public void Dispose() => _cts?.Dispose();
    }
}