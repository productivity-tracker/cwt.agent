﻿using System;
using System.IO;
using System.Reflection;
using NLog;
using Squirrel;
using Topshelf;
using Topshelf.Squirrel.Windows;
using Topshelf.Squirrel.Windows.Interfaces;

namespace CWT.Agent.Supervisor.Service
{
    internal class Program
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="updatePath">Path to update server</param>
        private static void Main(string updatePath=null)
        {
            AppDomain.CurrentDomain.UnhandledException += OnCurrentDomainUnhandledException;

            //This path should be replaced by one where to look for updates
            //var serviceUpdatePath = "https://pl.cwt.host:9990/releases";
            var serviceUpdatePath = @"C:\Updates";
            var appName = Assembly.GetExecutingAssembly().GetName().Name;
            var service = new ServiceImplementation();
            IUpdater selfUpdater = null;
            if (!Environment.UserInteractive)
            {
                var path = Path.Combine(serviceUpdatePath, appName);
                IUpdateManager updateManager = new UpdateManager(path);
                selfUpdater = new RepeatedTimeUpdater(updateManager).SetCheckUpdatePeriod(TimeSpan.FromMinutes(1));
            }

            var serviceName = appName;
            var host = new SquirreledHost(service, serviceName, serviceName, selfUpdater, withOverlapping: true);

            host.ConfigureAndRun(configurator =>
            {
                configurator.SetDescription("CWT Supervisor Service");
                configurator.EnableServiceRecovery(rc =>
                {
                    rc.RestartService(1);
                    rc.SetResetPeriod(1);
                });
                configurator.EnableSessionChanged();
                configurator.UseNLog();
            });
        }

        /// <summary>
        ///     Currents the domain_ unhandled exception.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.UnhandledExceptionEventArgs" /> instance containing the event data.</param>
        private static void OnCurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var report = (Exception)e.ExceptionObject;
            if (report != null)
            {
                _logger.Fatal(report);
            }
        }
    }
}