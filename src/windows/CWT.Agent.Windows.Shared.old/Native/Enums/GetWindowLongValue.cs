﻿namespace CWT.Agent.Windows.Shared.Native.Enums
{
    internal enum GetWindowLongValue
    {
        Style = -16,
        ExStyle = -20
    }
}