﻿using System;
using System.Runtime.InteropServices;

namespace CWT.Agent.Windows.Shared.Native.Structs
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    // ReSharper disable once InconsistentNaming
    internal struct RECT
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;

        public RECT(int dimension)
        {
            Left = Top = Right = Bottom = dimension;
        }
    }
}