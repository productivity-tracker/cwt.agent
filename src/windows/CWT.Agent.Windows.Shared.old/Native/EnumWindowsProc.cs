﻿using System;

namespace CWT.Agent.Windows.Shared.Native
{
    delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);
}