﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using CWT.Agent.Windows.Shared.Native.Enums;
using CWT.Agent.Windows.Shared.Native.Structs;

namespace CWT.Agent.Windows.Shared.Native
{
    public static class User32
    {
        private const string DllName = "user32.dll";

        [DllImport(DllName)]
        public static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport(DllName)]
        public static extern bool DrawIconEx(
            IntPtr hDC,
            int left,
            int top,
            IntPtr hIcon,
            int width,
            int height,
            int stepIfAniCur,
            IntPtr brushForFlickerFreeDraw,
            DrawIconExFlags flags);

        [DllImport(DllName)]
        public static extern WindowStyles GetWindowLong(IntPtr hWnd, GetWindowLongValue nIndex);

        [DllImport(DllName)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT rect);

        [DllImport(DllName)]
        public static extern bool IsWindow(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern IntPtr GetDesktopWindow();

        /// <summary>
        /// Gets a window handle (HWND) as an IntPtr to the currently active foreground window.
        /// </summary>
        /// <returns>Returns the window handle.</returns>
        [DllImport(DllName)]
        public static extern IntPtr GetForegroundWindow();

        /// <summary>
        /// Retrieves the process id of process that the window belongs to.
        /// </summary>
        /// <param name="windowHandle">the window handle to retrieve the process id for.</param>
        /// <param name="processId">the process id (return value).</param>
        /// <returns>The thread id that created the window.</returns>
        [DllImport(DllName, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern int GetWindowThreadProcessId(IntPtr windowHandle, out int processId);

        /// <summary>
        /// Gets the text of the window specified by the hWnd window handle.
        /// </summary>
        /// <param name="hWnd">the window handle used.</param>
        /// <param name="text">a <see cref="StringBuilder"/> used to store the text. At least <see cref="count"/> characters should be reserved in the string builder instance.</param>
        /// <param name="count">the buffer length.</param>
        /// <returns>returns the number of characters copied.</returns>
        [DllImport(DllName)]
        public static extern int GetWindowText(int hWnd, StringBuilder text, int count);

        [DllImport(DllName)]
        public static extern bool EnumWindows(EnumWindowsProc proc, IntPtr lParam);

        [DllImport(DllName)]
        public static extern bool EnumChildWindows(IntPtr hWnd, EnumWindowsProc proc, IntPtr lParam);

        [DllImport(DllName)]
        public static extern int GetWindowText(IntPtr hWnd, [Out] StringBuilder lpString, int nMaxCount);

        [DllImport(DllName)]
        public static extern IntPtr GetWindow(IntPtr hWnd, GetWindowEnum uCmd);

        [DllImport(DllName)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern bool IsIconic(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern bool IsZoomed(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern bool DestroyIcon(IntPtr hIcon);

        [DllImport(DllName)]
        public static extern IntPtr CopyIcon(IntPtr hIcon);

        [DllImport(DllName)]
        public static extern bool GetCursorInfo(ref CursorInfo pci);

        [DllImport(DllName)]
        public static extern bool GetIconInfo(IntPtr hIcon, out IconInfo piconinfo);

        [DllImport(DllName)]
        public static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport(DllName)]
        public static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport(DllName)]
        public static extern bool FillRect(IntPtr hDC, ref RECT Rect, IntPtr Brush);

        [DllImport(DllName)]
        public static extern bool SetWindowPos(
            IntPtr hWnd,
            IntPtr hWndInsertAfter,
            int x,
            int y,
            int cx,
            int cy,
            SetWindowPositionFlags wFlags);

        [DllImport(DllName)]
        public static extern int ShowWindow(IntPtr hWnd, int nCmdShow);
    }
}