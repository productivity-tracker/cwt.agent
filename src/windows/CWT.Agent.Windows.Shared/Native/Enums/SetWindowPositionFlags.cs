﻿namespace CWT.Agent.Windows.Shared.Native.Enums
{
    public enum SetWindowPositionFlags
    {
        ShowWindow = 0x400,
        NoActivate = 0x0010
    }
}