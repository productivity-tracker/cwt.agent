﻿namespace CWT.Agent.Windows.Shared.Native.Enums
{
    public enum GetWindowLongValue
    {
        Style = -16,
        ExStyle = -20
    }
}