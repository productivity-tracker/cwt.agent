﻿using System;

namespace CWT.Agent.Windows.Shared.Native
{
    public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);
}